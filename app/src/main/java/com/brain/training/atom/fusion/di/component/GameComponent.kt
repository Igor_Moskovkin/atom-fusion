package com.brain.training.atom.fusion.di.component

import com.brain.training.atom.fusion.di.module.GameModule
import com.brain.training.atom.fusion.screen.game.view.GameActivity
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(GameModule::class))
interface GameComponent {

    fun inject(gameActivity: GameActivity)
}
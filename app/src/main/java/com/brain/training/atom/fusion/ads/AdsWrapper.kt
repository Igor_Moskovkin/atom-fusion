package com.brain.training.atom.fusion.ads

interface AdsWrapper {

    fun isLoaded(): Boolean

    fun show()

    fun onDestroy()
}

interface AdCloseListener {

    fun onAdClosed()
}
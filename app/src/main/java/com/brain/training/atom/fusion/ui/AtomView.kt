package com.brain.training.atom.fusion.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.graphics.Rect
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.brain.training.atom.fusion.R
import com.brain.training.atom.fusion.model.Atom
import com.brain.training.atom.fusion.model.Element

class AtomView(context: Context, atom: Atom, private val atomSide: Int) : View(context) {
    var atom = atom
        set(value) {
            field = value
            backgroundFillColor = ContextCompat.getColor(context, atom.element.color)
            val hsv = FloatArray(3)
            Color.colorToHSV(ContextCompat.getColor(context, atom.element.color), hsv)
            hsv[2] = hsv[2] * 0.8f

            borderColor = Color.HSVToColor(255, hsv)
            invalidate()
        }
    private var backgroundFillColor: Int = 0
    private var borderColor: Int = 0
    private val textPrimaryPaint: Paint = Paint(ANTI_ALIAS_FLAG)
    private val textSecondaryPaint: Paint = Paint(ANTI_ALIAS_FLAG)
    private val paint: Paint = Paint(ANTI_ALIAS_FLAG)
    private val bounds = Rect()

    init {
        textPrimaryPaint.apply {
            textSize = resources.getDimensionPixelSize(R.dimen.atom_primary_text_size).toFloat()
            color = ContextCompat.getColor(context, R.color.atom_text_color_primary)
            textAlign = Paint.Align.CENTER
            typeface = ResourcesCompat.getFont(context, R.font.sf_text_medium)
            strokeWidth = resources.getDimensionPixelSize(R.dimen.minus_width).toFloat()
        }
        textSecondaryPaint.apply {
            textSize = resources.getDimensionPixelSize(R.dimen.atom_secondary_text_size).toFloat()
            color = ContextCompat.getColor(context, R.color.atom_text_color_secondary)
            textAlign = Paint.Align.CENTER
            typeface = ResourcesCompat.getFont(context, R.font.sf_text_regular)
        }
        pivotX = atomSide / 2F
        pivotY = atomSide / 2F

        backgroundFillColor = ContextCompat.getColor(context, atom.element.color)
        val hsv = FloatArray(3)
        Color.colorToHSV(ContextCompat.getColor(context, atom.element.color), hsv)
        hsv[2] = hsv[2] * 0.8f

        borderColor = Color.HSVToColor(255, hsv)

//        setLayerType(LAYER_TYPE_HARDWARE, null)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(atomSide, atomSide)
    }

    override fun onDraw(canvas: Canvas) {
        paint.color = borderColor
        canvas.drawCircle(atomSide / 2F, atomSide / 2F, (atomSide / 2).toFloat(), paint)
        paint.color = backgroundFillColor
        canvas.drawCircle(atomSide / 2F, atomSide / 2F, (atomSide / 2) * 0.95F, paint)

        textPrimaryPaint.getTextBounds(atom.element.letter, 0, 1, bounds)
        when (atom.element) {
            Element.MINUS -> {
                canvas.drawLine(
                        atomSide * 0.4F,
                        (atomSide / 2).toFloat(),
                        atomSide * 0.6F,
                        (atomSide / 2).toFloat(),
                        textPrimaryPaint)
            }
            else -> {
                canvas.drawText(
                        atom.element.letter,
                        (atomSide / 2).toFloat(),
                        (atomSide / 2 + bounds.height() / 2).toFloat(),
                        textPrimaryPaint)
            }
        }

        if (atom.element.atom > 0) {
            textSecondaryPaint.getTextBounds(atom.element.ordinal.toString(), 0, 1, bounds)
            canvas.drawText(
                    (atom.element.atom).toString(),
                    (atomSide / 2).toFloat(),
                    (atomSide - atomSide / 4 + bounds.height() / 2).toFloat(),
                    textSecondaryPaint)
        }
    }
}
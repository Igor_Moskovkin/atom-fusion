package com.brain.training.atom.fusion.di.module

import com.brain.training.atom.fusion.screen.splash.presenter.SplashPresenter
import com.brain.training.atom.fusion.screen.splash.view.SplashView
import dagger.Module
import dagger.Provides

@Module
class SplashModule {

    @Provides
    fun providePresenter(): SplashPresenter<SplashView> = SplashPresenter()
}
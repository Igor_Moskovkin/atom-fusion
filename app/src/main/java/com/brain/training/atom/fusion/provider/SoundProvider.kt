package com.brain.training.atom.fusion.provider

import android.annotation.TargetApi
import android.content.Context.AUDIO_SERVICE
import android.content.SharedPreferences
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import android.preference.PreferenceManager
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.R

private const val PLAY_SOUND_PREF = "play_sound_pref"

class SoundProvider(private val app: App) {
    private lateinit var soundPool: SoundPool
    private lateinit var soundPoolMap: MutableMap<Sound, Int>
    private val prefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(app)
    }
    private val audioManager: AudioManager by lazy {
        app.getSystemService(AUDIO_SERVICE) as AudioManager
    }

    init {
        createSoundPool()
        initSounds()
    }

    private fun createSoundPool() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            createNewSoundPool()
        } else {
            createOldSoundPool()
        }
    }

    @SuppressWarnings("deprecation")
    private fun createOldSoundPool() {
        soundPool = SoundPool(3, AudioManager.STREAM_MUSIC, 0)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun createNewSoundPool() {
        soundPool = SoundPool.Builder()
                .setAudioAttributes(AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_GAME)
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .build())
                .build()

    }

    private fun initSounds() {
        soundPoolMap = mutableMapOf()

        soundPoolMap[Sound.GAME_START] = soundPool.load(app, R.raw.start_game, 1)
    }

    fun isPlaySound(): Boolean {
        return prefs.getBoolean(PLAY_SOUND_PREF, true)
    }

    fun setPlaySound(playSound: Boolean) {
        prefs.edit().putBoolean(PLAY_SOUND_PREF, playSound).apply()
    }

    fun playSound(sound: Sound) {
        if (isPlaySound()) {
            val s = soundPoolMap[sound]
            val volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC).toFloat() / 15F
            s?.let {
                soundPool.play(s, volume, volume, 1, 0, 1F)
            }
        }
    }
}

enum class Sound {
    GAME_START,
    GAME_END,
}

package com.brain.training.atom.fusion.screen.game.view

import android.view.View
import com.brain.training.atom.fusion.model.Element
import com.brain.training.atom.fusion.screen.base.view.BaseView
import com.brain.training.atom.fusion.ui.AtomView

interface GameView : BaseView {
    var atoms: MutableList<AtomView>
    var highestAtom: Element
    var currentScore: Int

    fun openMainMenu()

    fun spawnStartAtoms()

    fun spawnAtom()

    fun placeAtom(view: View, angle: Int)

    fun rebalanceAtoms(view: View, angle: Float)

    fun setDirection(angle: Double)

    fun enableDirection(currentAngle: Double)

    fun disableDirection()

    fun getFusionAtoms(): List<AtomView>

    fun getFusionAtoms(atom : AtomView): List<AtomView>

    fun performFusion(atom: AtomView)

    fun enableTouch()

    fun disableTouch()

    fun getCurrentAtom(): AtomView

    fun getAtomByAngle(angle: Int): AtomView

    fun returnAtom(atom: AtomView)

    fun isAfterMinus(): Boolean

    fun getCurrentHighestAtom(): Element

    fun performGameEnd()

    fun startNewGame()

    fun showEndGameMenu()

    fun showBanner()

    fun hideBanner()

    fun shareViaIntent()

    fun pauseBanner()

    fun resumeBanner()

    fun destroyBanner()

    fun showStore()

    fun hideStore()

    fun showAdsIfNeeded(type: Int)

    fun spawnAccelerator()

    fun cloneAtom(atom: AtomView)

    fun showAtomStore()

    fun showPauseDialog()

    fun showSettings()
}
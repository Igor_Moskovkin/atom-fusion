package com.brain.training.atom.fusion.ads

import android.os.Bundle
import com.brain.training.atom.fusion.ANALYTICS_AD_SHOWN
import com.brain.training.atom.fusion.ANALYTICS_TOTAL_ADS_SHOWN
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.BUNDLE_KEY_NETWORK
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd

class AdmobWrapper(unitId: String, val scenario: String, var listener: AdCloseListener?) : AdsWrapper {
    private val interstitialAd: InterstitialAd by lazy {
        InterstitialAd(App.instance).apply {
            adUnitId = unitId
            adListener = object : AdListener() {

                override fun onAdClosed() {
                    requestNewInterstitial()
                    listener?.onAdClosed()
                }

                override fun onAdOpened() {
                    App.logEvent(ANALYTICS_TOTAL_ADS_SHOWN, Bundle())
                    App.logEvent(ANALYTICS_AD_SHOWN, Bundle().apply { putString(BUNDLE_KEY_NETWORK, scenario) })
                }
            }
        }
    }

    init {
        requestNewInterstitial()
    }

    private fun requestNewInterstitial() {
        interstitialAd.loadAd(AdRequest.Builder().build())
    }

    override fun isLoaded(): Boolean {
        return interstitialAd.isLoaded
    }

    override fun show() {
        interstitialAd.show()
    }

    override fun onDestroy() {
        listener = null
    }
}
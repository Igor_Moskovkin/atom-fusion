package com.brain.training.atom.fusion.di.component

import com.brain.training.atom.fusion.di.module.SplashModule
import com.brain.training.atom.fusion.screen.splash.view.SplashActivity
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(SplashModule::class))
interface SplashComponent {

    fun inject(splashActivity: SplashActivity)
}
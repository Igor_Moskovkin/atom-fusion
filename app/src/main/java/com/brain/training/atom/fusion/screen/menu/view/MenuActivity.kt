package com.brain.training.atom.fusion.screen.menu.view

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.constraintlayout.widget.ConstraintLayout
import com.brain.training.atom.fusion.*
import com.brain.training.atom.fusion.di.module.MenuModule
import com.brain.training.atom.fusion.dialog.DialogGift
import com.brain.training.atom.fusion.dialog.DialogLoading
import com.brain.training.atom.fusion.dialog.DialogSettings
import com.brain.training.atom.fusion.provider.*
import com.brain.training.atom.fusion.screen.base.view.BaseActivity
import com.brain.training.atom.fusion.screen.game.view.GameActivity
import com.brain.training.atom.fusion.screen.menu.presenter.MenuPresenter
import com.brain.training.atom.fusion.screen.tutorial.TutorialGame
import com.brain.training.atom.fusion.ui.AddPulseDrawable
import kotlinx.android.synthetic.main.activity_menu.*
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.solovyev.android.checkout.ProductTypes
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class MenuActivity : BaseActivity(), MenuView {
    override val layoutResId: Int = R.layout.activity_menu
    @Inject
    lateinit var presenter: MenuPresenter<MenuView>
    @Inject
    lateinit var billingProvider: BillingProvider
    @Inject
    lateinit var adsProvider: AdsProvider
    @Inject
    lateinit var playGamesProvider: PlayGamesProvider
    @Inject
    lateinit var soundProvider: SoundProvider
    @Inject
    lateinit var notificationProvider: NotificationProvider
    @Inject
    lateinit var giftProvider: GiftProvider
    @Inject
    lateinit var dataProvider: DataProvider
    private var loadingDialog: DialogLoading? = null
    private var settingsDialog: DialogSettings? = null
    private var giftDialog: DialogGift? = null
    private val mGiftHandler = Handler()
    private var mGiftRunnable: Runnable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (window.decorView.background as LayerDrawable).apply {
            (findDrawableByLayerId(R.id.circle) as GradientDrawable).apply {
                colors = intArrayOf(Color.TRANSPARENT, Color.TRANSPARENT)
                alpha = 45
            }
        }

        presenter.onAttach(this)
        presenter.onCreate()

        playBtn.onClick {
            presenter.onPlayClick()
        }
        leaderboardBtn.onClick {
            App.logEvent(ANALYTICS_LEADERBOARD_CLICK, Bundle())
            presenter.onLeaderboardClick()
        }

        settingsBtn.onClick {
            presenter.onSettingsClick()
        }
        rateBtn.onClick {
            App.logEvent(ANALYTICS_RATE_US_CLICK, Bundle())
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
            } catch (e: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
            }
        }

        removeAdsBtn.onClick {
            App.logEvent(ANALYTICS_REMOVE_ADS_CLICK, Bundle())
            billingProvider.launchPurchaseFlow(this@MenuActivity, ProductTypes.IN_APP, billingProvider.removeAdsSku)
        }

        gift.onClick {
            giftDialog = DialogGift(this@MenuActivity, giftProvider.getNextGift(), adsProvider)
            giftDialog?.setOnDismissListener {
                val gift = giftDialog?.getGift()
                gift?.let {
                    dataProvider.changeProtonsCount(it.protons)
                    dataProvider.changeAcceleratorsCount(it.accelerators)
                }
                giftProvider.scheduleNextGift()
                startGiftsTimer()
            }
            giftDialog?.show()
        }
    }

    override fun onStart() {
        super.onStart()
        playGamesProvider.onStart(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        playGamesProvider.onActivityResult(this, requestCode, resultCode, data)
        billingProvider.onActivityResult(this, requestCode, resultCode, data)
    }

    override fun onDestroy() {
        presenter.onDetach()

        settingsDialog?.dismiss()
        giftDialog?.dismiss()

        mGiftRunnable?.let {
            mGiftHandler.removeCallbacks(mGiftRunnable)
        }
        super.onDestroy()
    }

    override fun performDi() {
        App.instance.component.plus(MenuModule()).inject(this)
    }

    override fun setHighestScore(score: String) {
        highestScore.text = score
    }

    override fun startNormalGame() {
        startActivity(intentFor<GameActivity>())
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        finish()
    }

    override fun initProviders() {
        billingProvider.setActivity(this)
        adsProvider.subscribe(this)
    }

    override fun stopProviders() {
        billingProvider.destroy(this)
        adsProvider.unsubscribe(this)
    }

    override fun showLeaderboard() {
        playGamesProvider.showLeaderboard(this, getString(R.string.leaderboard_classic_mode))
    }

    override fun showLoadingDialog() {
        loadingDialog = DialogLoading(this)
        loadingDialog?.show()
    }

    override fun hideLoadingDialog() {
        loadingDialog?.dismiss()
    }

    override fun openSettings() {
        settingsDialog = DialogSettings(this, soundProvider, notificationProvider)
        settingsDialog?.show()
    }

    override fun startGiftsTimer() {
        val millis = giftProvider.getNextGiftTime()
        val df = SimpleDateFormat("H:mm:ss", Locale.getDefault())
        df.timeZone = TimeZone.getTimeZone("UTC")

        if (millis - System.currentTimeMillis() > 0) {
            gift.alpha = 0.3F
            gift.isEnabled = false
            giftPulse.backgroundDrawable = null
            giftTimer.visibility = View.VISIBLE
            mGiftRunnable = object : Runnable {
                override fun run() {
                    giftTimer.text = df.format(Date(millis - System.currentTimeMillis()))

                    if (millis - System.currentTimeMillis() >= 0) {
                        mGiftHandler.postDelayed(this, 1000)
                    } else {
                        mGiftHandler.removeCallbacks(this)
                        gift.alpha = 1F
                        startGiftPulse()
                        giftTimer.visibility = GONE
                    }
                }
            }
            mGiftHandler.post(mGiftRunnable)
        } else {
            gift.alpha = 1F
            gift.isEnabled = true
            startGiftPulse()
            giftTimer.visibility = GONE
        }
    }

    private fun startGiftPulse() {
        gift.post {
            giftPulse.apply {
                backgroundDrawable = AddPulseDrawable(Color.WHITE)
                layoutParams = ConstraintLayout.LayoutParams((gift.width * 1.3).toInt(), (gift.height * 1.3).toInt()).apply {
                    leftToLeft = gift.id
                    topToTop = gift.id
                    bottomToBottom = gift.id
                    rightToRight = gift.id
                }
            }
        }
    }

    override fun onBackPressed() {
        finish()
    }

    override fun updateUi() {
        removeAdsBtn.visibility = if (adsProvider.canShowAds()) {
            VISIBLE
        } else {
            GONE
        }
    }

    override fun startTutorial() {
        startActivity(intentFor<TutorialGame>())
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        finish()
    }
}
package com.brain.training.atom.fusion.di.module

import com.brain.training.atom.fusion.provider.AdsProvider
import com.brain.training.atom.fusion.provider.DataProvider
import com.brain.training.atom.fusion.screen.menu.presenter.MenuPresenter
import com.brain.training.atom.fusion.screen.menu.view.MenuView
import dagger.Module
import dagger.Provides

@Module
class MenuModule {

    @Provides
    fun providePresenter(dataProvider: DataProvider, adsProvider: AdsProvider): MenuPresenter<MenuView> = MenuPresenter(dataProvider, adsProvider)
}
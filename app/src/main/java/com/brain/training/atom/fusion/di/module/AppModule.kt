package com.brain.training.atom.fusion.di.module

import androidx.room.Room
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.provider.*
import com.brain.training.atom.fusion.provider.database.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideAppContext() = app

    @Provides
    @Singleton
    fun provideRemoteConfig(): RemoteConfig = RemoteConfigImpl()

    @Provides
    @Singleton
    fun provideAppDatabase(app: App): AppDatabase =
            Room.databaseBuilder(app, AppDatabase::class.java, "atom_fusion.db").allowMainThreadQueries().build()

    @Provides
    @Singleton
    fun provideDataProvider(database: AppDatabase): DataProvider = DataProviderImpl(database)

    @Provides
    @Singleton
    fun provideAdsProvider(app: App, remoteConfig: RemoteConfig, dataProvider: DataProvider, playGamesProvider: PlayGamesProvider): AdsProvider = AdmobProvider(app, remoteConfig, dataProvider, playGamesProvider)

    @Provides
    @Singleton
    fun provideBillingProvider(app: App, dataProvider: DataProvider): BillingProvider {
//        val installerId: String? = app.packageManager.getInstallerPackageName(app.packageName)
//
//        installerId?.let {
//            return when (it) {
//                BillingProvider.GOOGLE_PLAY_SOURCE -> GoogleBillingProvider(app, dataProvider)
//                else -> GoogleBillingProvider(app, dataProvider)
//            }
//        }

        return GoogleBillingProvider(app, dataProvider)
    }

    @Provides
    @Singleton
    fun providePlayGamesProvider(app: App, authProvider: AuthProvider): PlayGamesProvider = PlayGamesProvider(app, authProvider)

    @Provides
    @Singleton
    fun provideAuthProvider(app: App): AuthProvider = FirebaseAuthProvider(app)

    @Provides
    @Singleton
    fun provideSoundProvider(app: App): SoundProvider = SoundProvider(app)

    @Provides
    @Singleton
    fun provideNotificationProvider(app: App): NotificationProvider = NotificationProvider(app)

    @Provides
    @Singleton
    fun provideGiftProvider(app: App, notificationProvider: NotificationProvider): GiftProvider = GiftProvider(app, notificationProvider)
}
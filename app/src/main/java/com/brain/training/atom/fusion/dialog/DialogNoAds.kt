package com.brain.training.atom.fusion.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialog
import com.brain.training.atom.fusion.R
import kotlinx.android.synthetic.main.dialog_no_ads.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.textResource

class DialogNoAds(context: Context) : AppCompatDialog(context, R.style.AppTheme_FullScreen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_no_ads)

        window?.attributes?.dimAmount = 0.6f

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)

        messageView.textResource = R.string.no_video_ads_message

        okBtn.onClick {
            dismiss()
        }
    }
}
package com.brain.training.atom.fusion.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialog
import com.brain.training.atom.fusion.R
import com.brain.training.atom.fusion.model.Gift
import com.brain.training.atom.fusion.provider.AdsProvider
import com.brain.training.atom.fusion.provider.VideoAdListener
import com.brain.training.atom.fusion.screen.base.view.BaseActivity
import com.github.florent37.viewanimator.ViewAnimator
import kotlinx.android.synthetic.main.dialog_gift.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class DialogGift(
        private val baseActivity: BaseActivity,
        private val gift: Gift,
        private val adsProvider: AdsProvider) : AppCompatDialog(baseActivity, R.style.AppTheme_FullScreen) {

    private var loadingDialog: DialogLoading? = null
    private var noAdsDialog: DialogNoAds? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_gift)

        window?.attributes?.dimAmount = 0.6f

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        setCancelable(false)

        okBtn.onClick {
            dismiss()
        }

        doubleBtn.onClick {
            loadingDialog = DialogLoading(getContext())
            loadingDialog?.setOnDismissListener { loadingDialog = null }
            loadingDialog?.show()

            adsProvider.showVideoAd(object : VideoAdListener {
                override fun onRewarded() {
                    gift.accelerators *= 2
                    gift.protons *= 2

                    doubleBtn.visibility = GONE

                    loadingDialog?.dismiss()

                    updateUi()
                }

                override fun onLoaded() {
                    loadingDialog?.let {
                        adsProvider.showVideoAd(this)
                        it.dismiss()
                    }
                }

                override fun onFailedToLoad() {
                    noAdsDialog = DialogNoAds(baseActivity)
                    noAdsDialog?.show()
                    loadingDialog?.dismiss()
                }
            })
        }

        giftContainer.postDelayed({
            ViewAnimator.animate(dialogRoot)
                    .duration(500)
                    .onStop {
                        loadingContainer.visibility = GONE

                        giftTitle.visibility = VISIBLE
                        giftContainer.visibility = VISIBLE
                        buttonsContainer.visibility = VISIBLE

                        updateUi()
                    }
                    .scale(1F, 0F)
                    .thenAnimate(dialogRoot)
                    .duration(500)
                    .scale(0F, 1F)
                    .start()
                    .start()
        }, 1500)
    }

    private fun updateUi() {
        if (gift.accelerators == 0) {
            giftAccelerators.visibility = GONE
        } else {
            giftAccelerators.text = context.getString(R.string.gift_accelerators_pattern).format(gift.accelerators)
        }
        giftProtons.text = context.getString(R.string.gift_protons_pattern).format(gift.protons)
    }

    fun getGift(): Gift {
        return gift
    }
}
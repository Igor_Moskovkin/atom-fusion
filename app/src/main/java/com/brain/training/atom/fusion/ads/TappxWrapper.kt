package com.brain.training.atom.fusion.ads

import android.os.Bundle
import com.brain.training.atom.fusion.ANALYTICS_AD_SHOWN
import com.brain.training.atom.fusion.ANALYTICS_TOTAL_ADS_SHOWN
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.BUNDLE_KEY_NETWORK
import com.tappx.sdk.android.TappxAdError
import com.tappx.sdk.android.TappxInterstitial
import com.tappx.sdk.android.TappxInterstitialListener

class TappxWrapper(unitId: String, val scenario: String, var listener: AdCloseListener?) : AdsWrapper {
    private val interstitialAd: TappxInterstitial by lazy {
        TappxInterstitial(App.instance, unitId).apply {
            setListener(object : TappxInterstitialListener {
                override fun onInterstitialLoaded(p0: TappxInterstitial?) {
                }

                override fun onInterstitialShown(p0: TappxInterstitial?) {
                    App.logEvent(ANALYTICS_TOTAL_ADS_SHOWN, Bundle())
                    App.logEvent(ANALYTICS_AD_SHOWN, Bundle().apply { putString(BUNDLE_KEY_NETWORK, scenario) })
                }

                override fun onInterstitialDismissed(p0: TappxInterstitial?) {
                    requestNewInterstitial()
                    listener?.onAdClosed()
                }

                override fun onInterstitialLoadFailed(p0: TappxInterstitial?, p1: TappxAdError?) {
                }

                override fun onInterstitialClicked(p0: TappxInterstitial?) {
                }
            })
        }
    }

    init {
        requestNewInterstitial()
    }

    private fun requestNewInterstitial() {
        interstitialAd.loadAd()
    }

    override fun isLoaded(): Boolean {
        return interstitialAd.isReady
    }

    override fun show() {
        interstitialAd.show()
    }

    override fun onDestroy() {
        interstitialAd.destroy()
        listener = null
    }
}
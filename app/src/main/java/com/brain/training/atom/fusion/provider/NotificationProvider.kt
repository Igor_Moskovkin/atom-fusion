package com.brain.training.atom.fusion.provider

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.preference.PreferenceManager
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_MAX
import androidx.core.app.TaskStackBuilder
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.R
import com.brain.training.atom.fusion.provider.GiftProvider.Companion.REMINDER_GIFT_ACCELERATORS_COUNT
import com.brain.training.atom.fusion.provider.GiftProvider.Companion.REMINDER_GIFT_PROTONS_COUNT
import com.brain.training.atom.fusion.receiver.NotificationReceiver
import com.brain.training.atom.fusion.screen.splash.view.SplashActivity
import java.util.*


class NotificationProvider(val app: App) {
    private val prefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(app)
    }

    fun scheduleGiftNotification(delay: Long) {
        val notificationIntent = Intent(app, NotificationReceiver::class.java)
        notificationIntent.putExtra(NotificationReceiver.NOTIFICATION_ID, GIFT_NOTIFICATION_ID)
        notificationIntent.putExtra(NotificationReceiver.NOTIFICATION, getGiftNotification())
        val pendingIntent = PendingIntent.getBroadcast(
                app,
                0,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmManager = app.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.RTC_WAKEUP, delay, pendingIntent)
    }

    @SuppressLint("NewApi")
    fun getGiftNotification(): Notification {
        val intent = Intent(app, SplashActivity::class.java)

        val stackBuilder = TaskStackBuilder.create(app)
        stackBuilder.addParentStack(SplashActivity::class.java)
        stackBuilder.addNextIntent(intent)
        val pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(app, GIFTS_CHANNEL_ID)
        builder.setContentTitle(app.getString(R.string.gift_notification_title))
        builder.setContentText(app.getString(R.string.gift_notification_subtitle))
        builder.setSmallIcon(R.drawable.ic_notification_icon)
        builder.setContentIntent(pendingIntent)
        builder.setAutoCancel(true)
        builder.priority = PRIORITY_MAX

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = app.getString(R.string.channel_name)
            val description = app.getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(GIFTS_CHANNEL_ID, name, importance)
            mChannel.description = description

            val notificationManager = app.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }

        return builder.build()
    }

    fun isShowNotification(): Boolean {
        return prefs.getBoolean(SHOW_NOTIFICATION_KEY, true)
    }

    fun setShowNotification(showNotification: Boolean) {
        prefs.edit().putBoolean(SHOW_NOTIFICATION_KEY, showNotification).apply()
    }

    fun scheduleRemindNotification() {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, 2)
        calendar.set(Calendar.HOUR_OF_DAY, 22)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)

        val notificationIntent = Intent(app, NotificationReceiver::class.java)
        notificationIntent.putExtra(NotificationReceiver.NOTIFICATION_ID, REMINDER_NOTIFICATION_ID)
        notificationIntent.putExtra(NotificationReceiver.NOTIFICATION, getRemindNotification())
        val pendingIntent = PendingIntent.getBroadcast(
                app,
                1,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmManager = app.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
    }

    @SuppressLint("NewApi")
    fun getRemindNotification(): Notification {
        val intent = Intent(app, SplashActivity::class.java)
        intent.putExtra(FROM_REMINDER_KEY, true)

        val stackBuilder = TaskStackBuilder.create(app)
        stackBuilder.addParentStack(SplashActivity::class.java)
        stackBuilder.addNextIntent(intent)
        val pendingIntent = stackBuilder.getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(app, GIFTS_CHANNEL_ID)
        builder.setContentTitle(app.getString(R.string.remind_notification_title).format(
                REMINDER_GIFT_ACCELERATORS_COUNT,
                REMINDER_GIFT_PROTONS_COUNT))
        builder.setContentText(app.getString(R.string.remind_notification_subtitle))
        builder.setSmallIcon(R.drawable.ic_notification_icon)
        builder.setContentIntent(pendingIntent)
        builder.setAutoCancel(true)
        builder.priority = PRIORITY_MAX

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = app.getString(R.string.channel_name)
            val description = app.getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(GIFTS_CHANNEL_ID, name, importance)
            mChannel.description = description

            val notificationManager = app.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }

        return builder.build()
    }

    companion object {
        const val SHOW_NOTIFICATION_KEY = "show_notification_pref"
        const val GIFTS_CHANNEL_ID = "gift_channel_id"
        const val GIFT_NOTIFICATION_ID = 3213
        const val REMINDER_NOTIFICATION_ID = 2453
        const val FROM_REMINDER_KEY = "from_reminder"
        const val REMINDER_NOTIFICATION_SHOWN = "reminder_notification_shown"
    }
}
package com.brain.training.atom.fusion.ui

import android.content.Context
import android.graphics.*
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.util.AttributeSet
import android.view.View
import com.brain.training.atom.fusion.R


class DirectionView : View {
    var mainColor: Int = Color.TRANSPARENT
    set(value) {
        field = value
        invalidate()
    }
    private val radius: Float by lazy {
        width / 2.1F
    }
    private var centerX: Float = 0F
    private var centerY: Float = 0F
    var drawDirection = false
    private val circlePaint = Paint(ANTI_ALIAS_FLAG)
    private val linePaint = Paint(ANTI_ALIAS_FLAG)
    var angle:Double = 0.0
    set(value) {
        field = value
        invalidate()
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        circlePaint.apply {
            style = Paint.Style.STROKE
            color = mainColor
            strokeWidth = resources.getDimensionPixelSize(R.dimen.direction_circle_width).toFloat()
        }
        linePaint.apply {
            style = Paint.Style.STROKE
            color = Color.RED
            strokeWidth = resources.getDimensionPixelSize(R.dimen.direction_circle_width).toFloat()
        }

//        setLayerType(LAYER_TYPE_HARDWARE, null)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        centerX = MeasureSpec.getSize(widthMeasureSpec) / 2F
        centerY = MeasureSpec.getSize(heightMeasureSpec) / 2F

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas) {
        if (drawDirection) {
            linePaint.shader = LinearGradient(
                    centerX,
                    centerY,
                    (centerX + Math.cos((angle - 90) * Math.PI / 180) * radius * 0.75).toFloat(),
                    (centerY + Math.sin((angle - 90) * Math.PI / 180) * radius * 0.75).toFloat(),
                    mainColor,
                    Color.TRANSPARENT,
                    Shader.TileMode.CLAMP)

            canvas.drawLine(
                    centerX,
                    centerY,
                    (centerX + Math.cos((angle - 90) * Math.PI / 180) * radius).toFloat(),
                    (centerY + Math.sin((angle - 90) * Math.PI / 180) * radius).toFloat(),
                    linePaint)
        }

        circlePaint.color = mainColor
        canvas.drawCircle(centerX, centerY, radius, circlePaint)
    }
}
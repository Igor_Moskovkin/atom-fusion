package com.brain.training.atom.fusion.ui

import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.view.animation.DecelerateInterpolator

class AddPulseDrawable(color: Int) : PulseDrawable(color) {
    override val pulseStartColorOpacity = 0
    override val minimumRadius = 0f
    override val animationDurationInMs = 1500
    override var currentExpandAnimationValue = 0F
    override var currentAlphaAnimationValue = 255

    init {
        initializeDrawable()
    }

    override fun prepareAnimation() {
        val expandAnimator = ValueAnimator.ofFloat(0f, 1f)
        expandAnimator.repeatCount = ValueAnimator.INFINITE
        expandAnimator.repeatMode = ValueAnimator.RESTART
        expandAnimator.addUpdateListener { animation ->
            currentExpandAnimationValue = animation.animatedValue as Float
            if (currentExpandAnimationValue == 0f) {
                currentAlphaAnimationValue = 255
            }
            invalidateSelf()
        }
        val alphaAnimator = ValueAnimator.ofInt(255, 0)
        alphaAnimator.startDelay = (animationDurationInMs / 4).toLong()
        alphaAnimator.repeatCount = ValueAnimator.INFINITE
        alphaAnimator.repeatMode = ValueAnimator.RESTART
        alphaAnimator.addUpdateListener { animation -> currentAlphaAnimationValue = animation.animatedValue as Int }
        val animation = AnimatorSet()
        animation.playTogether(expandAnimator, alphaAnimator)
        animation.duration = animationDurationInMs.toLong()
        animation.interpolator = DecelerateInterpolator()
        animation.start()
    }
}
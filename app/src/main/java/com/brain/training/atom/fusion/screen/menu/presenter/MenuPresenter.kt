package com.brain.training.atom.fusion.screen.menu.presenter

import android.preference.PreferenceManager
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.provider.AdsProvider
import com.brain.training.atom.fusion.provider.DataProvider
import com.brain.training.atom.fusion.screen.base.presenter.BasePresenter
import com.brain.training.atom.fusion.screen.menu.view.MenuView

internal const val TUTORIAL_SHOWN = "tutorial_shown"

class MenuPresenter<V : MenuView>(private val dataProvider: DataProvider, private val adsProvider: AdsProvider) : BasePresenter<V>() {

    override fun onAttach(view: V?) {
        super.onAttach(view)
        getView()?.initProviders()
    }

    fun onPlayClick() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(App.instance)
        if (!prefs.getBoolean(TUTORIAL_SHOWN, false)) {
            getView()?.startTutorial()
        } else {
            getView()?.startNormalGame()
        }
    }

    fun onCreate() {
        getView()?.let {
            it.setHighestScore(dataProvider.getUser().standardModeScore.toString())
            it.startGiftsTimer()
        }
    }

    override fun onDetach() {
        getView()?.let {
            it.stopProviders()
            it.hideLoadingDialog()
        }
        super.onDetach()
    }

    fun onLeaderboardClick() {
        getView()?.showLeaderboard()
    }

    fun onSettingsClick() {
        getView()?.openSettings()
    }
}
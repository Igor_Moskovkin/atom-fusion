package com.brain.training.atom.fusion.ui

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.view.MotionEvent.ACTION_DOWN
import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.brain.training.atom.fusion.R
import com.brain.training.atom.fusion.model.TutorialCircle


class TutorialView(context: Context, private val mCircles: List<TutorialCircle>, private val mText: String) : View(context) {
    private val mViewPaint: Paint = Paint(ANTI_ALIAS_FLAG)
    private val mBackgroundPaint: Paint by lazy {
        Paint(ANTI_ALIAS_FLAG).apply {
            color = Color.parseColor("#99000000")
        }
    }
    private val mTransparentPaint: Paint by lazy {
        Paint(ANTI_ALIAS_FLAG).apply {
            color = Color.TRANSPARENT
            xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        }
    }
    private val mTextPaint: Paint by lazy {
        Paint(ANTI_ALIAS_FLAG).apply {
            color = Color.WHITE
            textAlign = Paint.Align.CENTER
            typeface = ResourcesCompat.getFont(context, R.font.sf_text_medium)
            textSize = resources.getDimensionPixelSize(R.dimen.tutorial_text_size).toFloat()
        }
    }
    private var mBitmap: Bitmap? = null
    private var mCnvs: Canvas? = null
    private val touchRect: RectF by lazy {
        RectF(mCircles[0].x - mCircles[0].radius, mCircles[0].y - mCircles[0].radius, mCircles[0].x + mCircles[0].radius, mCircles[0].y + mCircles[0].radius)
    }

    init {
        this.setOnTouchListener { _, event ->
            !(event?.action == ACTION_DOWN && touchRect.contains(event.x, event.y))
        }
    }

    @SuppressLint("DrawAllocation")
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        mBitmap = Bitmap.createBitmap(View.MeasureSpec.getSize(widthMeasureSpec), View.MeasureSpec.getSize(heightMeasureSpec), Bitmap.Config.ARGB_8888)

        mBitmap?.let {
            mCnvs = Canvas(it)
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        mBitmap = null
        mCnvs = null
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (mBitmap == null) {
            return
        }

        mCnvs?.let {
            it.drawRect(0F, 0F, it.width.toFloat(), it.height.toFloat(), mBackgroundPaint)
            for ((x, y, radius) in mCircles) {
                it.drawCircle(x, y, radius, mTransparentPaint)
            }
        }
        mBitmap?.let {
            canvas.drawBitmap(it, 0F, 0F, mViewPaint)
        }

        val x = width / 2F
        var y = height / 5F

        for (line in mText.split("\n")) {
            canvas.drawText(line, x, y, mTextPaint)
            y += mTextPaint.descent() - mTextPaint.ascent()
        }
    }
}
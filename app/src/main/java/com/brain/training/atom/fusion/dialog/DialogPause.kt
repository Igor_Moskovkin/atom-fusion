package com.brain.training.atom.fusion.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialog
import com.brain.training.atom.fusion.R
import kotlinx.android.synthetic.main.dialog_pause.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class DialogPause(
        context: Context,
        private val restartListener: View.OnClickListener,
        private val homeListener: View.OnClickListener,
        private val settingsListener: View.OnClickListener) : AppCompatDialog(context, R.style.AppTheme_FullScreen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_pause)

        window?.attributes?.dimAmount = 0.6f

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)

        close.onClick {
            dismiss()
        }
        backBtn.onClick {
            dismiss()
        }
        homeBtn.onClick {
            homeListener.onClick(null)
            dismiss()
        }
        restartBtn.onClick {
            restartListener.onClick(null)
            dismiss()
        }
        settingsBtn.onClick {
            settingsListener.onClick(null)
            dismiss()
        }
    }
}
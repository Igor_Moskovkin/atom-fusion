package com.brain.training.atom.fusion.ui

import android.graphics.*
import android.graphics.drawable.Drawable

abstract class PulseDrawable(private val color: Int) : Drawable() {
    open val pulseStartColorOpacity: Int = 0
    open val minimumRadius = 0F
    open val animationDurationInMs = 1500

    private val pulsePaint: Paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG).apply {
            style = Paint.Style.FILL
        }
    }
    private var fullSizeRadius: Float = 0F
    open var currentExpandAnimationValue = 0F
    open var currentAlphaAnimationValue = 255

    private val pulseColor: Int
        get() = Color.argb(currentAlphaAnimationValue, Color.red(color), Color.green(color), Color.blue(color))

    fun initializeDrawable() {
        prepareAnimation()
    }

    abstract fun prepareAnimation()

    override fun setAlpha(alpha: Int) {
        pulsePaint.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {}

    override fun getOpacity(): Int {
        return pulsePaint.alpha
    }

    override fun draw(canvas: Canvas) {
        val bounds = bounds
        val centerX = bounds.exactCenterX()
        val centerY = bounds.exactCenterY()
        calculateFullSizeRadius()
        preparePaintShader()
        renderPulse(canvas, centerX, centerY)
    }

    private fun renderPulse(canvas: Canvas, centerX: Float, centerY: Float) {
        val currentRadius = fullSizeRadius * currentExpandAnimationValue
        if (currentRadius > minimumRadius) {
            canvas.drawCircle(centerX, centerY, currentRadius, pulsePaint)
        }
    }

    private fun preparePaintShader() {
        val bounds = bounds
        val centerX = bounds.exactCenterX()
        val centerY = bounds.exactCenterY()
        val radius = Math.min(bounds.width(), bounds.height()) / 2
        if (radius > minimumRadius) {
            val edgeColor = pulseColor
            val centerColor = Color.argb(pulseStartColorOpacity, Color.red(color),
                    Color.green(color),
                    Color.blue(color))
            pulsePaint.shader = RadialGradient(centerX, centerY, radius.toFloat(),
                    centerColor, edgeColor, Shader.TileMode.CLAMP)
        } else {
            pulsePaint.shader = null
        }
    }

    private fun calculateFullSizeRadius() {
        val bounds = bounds
        val minimumDiameter = Math.min(bounds.width(), bounds.height())
        fullSizeRadius = (minimumDiameter / 2).toFloat()
    }
}
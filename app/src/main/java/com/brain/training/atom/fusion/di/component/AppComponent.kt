package com.brain.training.atom.fusion.di.component

import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.di.module.AppModule
import com.brain.training.atom.fusion.di.module.GameModule
import com.brain.training.atom.fusion.di.module.MenuModule
import com.brain.training.atom.fusion.di.module.SplashModule
import com.brain.training.atom.fusion.receiver.NotificationReceiver
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {

    fun inject(app: App)

    fun inject(notificationReceiver: NotificationReceiver)

    fun plus(splashModule: SplashModule): SplashComponent

    fun plus(menuModule: MenuModule): MenuComponent

    fun plus(gameModule: GameModule): GameComponent
}
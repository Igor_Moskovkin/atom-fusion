package com.brain.training.atom.fusion.screen.base.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate

abstract class BaseActivity : AppCompatActivity(), BaseView {
    abstract val layoutResId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        setContentView(layoutResId)
        performDi()
    }

    override fun onResume() {
        super.onResume()
        isInForeground = true
    }

    override fun onPause() {
        isInForeground = false
        super.onPause()
    }

    abstract fun performDi()

    override fun updateUi() {}

    open fun onAdClosed() {}

    override fun initProviders() {}

    override fun stopProviders() {}

    companion object {
        var isInForeground: Boolean = false
    }
}

package com.brain.training.atom.fusion.provider

import android.app.Activity
import com.brain.training.atom.fusion.App
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PlayGamesAuthProvider


class FirebaseAuthProvider(val app: App) : AuthProvider {
    private val firebaseAuth = FirebaseAuth.getInstance()

    override fun signIn(activity: Activity, account: GoogleSignInAccount) {
        if (account.serverAuthCode != null) {
            firebaseAuth.signInWithCredential(PlayGamesAuthProvider.getCredential(account.serverAuthCode!!))
        }
    }
}

interface AuthProvider {

    fun signIn(activity: Activity, account: GoogleSignInAccount)
}
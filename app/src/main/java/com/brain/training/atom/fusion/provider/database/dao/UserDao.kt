package com.brain.training.atom.fusion.provider.database.dao

import androidx.room.*
import androidx.room.OnConflictStrategy.ABORT
import androidx.room.OnConflictStrategy.REPLACE
import com.brain.training.atom.fusion.provider.database.model.User

@Dao
interface UserDao {

    @Query("SELECT count(*) FROM user")
    fun getUsersCount(): Int

    @Query("SELECT * FROM user LIMIT 1")
    fun getUser(): User

    @Insert(onConflict = ABORT)
    fun insertUser(user: User)

    @Update(onConflict = REPLACE)
    fun updateUser(user: User)

    @Delete
    fun deleteUser(user: User)
}
package com.brain.training.atom.fusion.adapter

import android.animation.AnimatorInflater
import android.os.Build
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.R
import com.brain.training.atom.fusion.model.Atom
import com.brain.training.atom.fusion.model.Element
import com.brain.training.atom.fusion.provider.DataProvider
import com.brain.training.atom.fusion.ui.AtomView
import kotlinx.android.synthetic.main.item_atom_store.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class AtomStoreAdapter(
        private val dataProvider: DataProvider,
        private val atomSide: Int,
        private val highestAtom: Element,
        private val listener: AtomStoreListener) : RecyclerView.Adapter<AtomStoreAdapter.ViewHolder>() {
    private val atoms = mutableListOf<Atom>()

    init {
        val elements = Element.values()

        elements
                .filter { it.atom > 0 }
                .mapTo(atoms) { Atom(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val holder = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_atom_store, parent, false))

        val atom = AtomView(parent.context, Atom(Element.H), atomSide).apply {
            id = R.id.storeAtom
            layoutParams = FrameLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT, Gravity.CENTER).apply {
                topMargin = App.instance.resources.getDimensionPixelSize(R.dimen.atom_store_margin)
                bottomMargin = App.instance.resources.getDimensionPixelSize(R.dimen.atom_store_margin)
                marginEnd = App.instance.resources.getDimensionPixelSize(R.dimen.atom_store_margin)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    stateListAnimator = AnimatorInflater.loadStateListAnimator(parent.context, R.animator.button_selector)
                }
            }
        }

        holder.itemView.root.addView(atom)
        holder.itemView.onClick {
            val element = atoms[holder.adapterPosition].element

            if (dataProvider.getProtonsCount() >= element.atom && element.atom <= highestAtom.atom) {
                listener.onAtomClick(element)
            }
        }

        return holder
    }

    override fun getItemCount(): Int {
        return atoms.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(atoms[position], if (atoms[position].element.atom <= highestAtom.atom) {1F} else {0.1F})
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val atomView: AtomView by lazy {
            itemView.findViewById<AtomView>(R.id.storeAtom)
        }

        fun bind(atom: Atom, alpha: Float) {
            itemView.root.alpha = alpha
            atomView.atom = atom
            itemView.price.text = atom.element.atom.toString()
        }

    }
}

interface AtomStoreListener {

    fun onAtomClick(element: Element)
}
package com.brain.training.atom.fusion.screen.game.presenter

import android.app.Activity
import android.os.Handler
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.R
import com.brain.training.atom.fusion.model.Element
import com.brain.training.atom.fusion.provider.*
import com.brain.training.atom.fusion.screen.base.presenter.BasePresenter
import com.brain.training.atom.fusion.screen.game.view.ADS_TYPE_HOME
import com.brain.training.atom.fusion.screen.game.view.ADS_TYPE_RESTART
import com.brain.training.atom.fusion.screen.game.view.GameView
import com.brain.training.atom.fusion.ui.AtomView

private const val MAX_ATOMS_COUNT = 19

class GamePresenter<V : GameView>(
        private val dataProvider: DataProvider,
        private val adsProvider: AdsProvider,
        private val remoteConfigProvider: RemoteConfig,
        private val playGamesProvider: PlayGamesProvider,
        private val soundProvider: SoundProvider) : BasePresenter<V>() {

    fun onCreate() {
        getView()?.let {
            if (adsProvider.canShowAds() && remoteConfigProvider.showGameBanner) {
                it.showBanner()
            } else {
                it.hideBanner()
            }

            it.spawnStartAtoms()

            soundProvider.playSound(Sound.GAME_START)

            Handler().postDelayed({
                it.spawnAtom()
                it.enableTouch()
            }, 500)
        }
    }

    fun onResume() {
        getView()?.let {
            if (adsProvider.canShowAds()) {
                it.resumeBanner()
            }
        }
    }

    fun onPause() {
        getView()?.let {
            val user = dataProvider.getUser()
            if (user.standardModeScore < it.currentScore) {
                user.standardModeScore = it.currentScore
                dataProvider.updateUser(user)
            }
            if (adsProvider.canShowAds()) {
                it.pauseBanner()
            }
        }
    }

    fun onDirectionMove(angle: Double) {
        getView()?.setDirection(angle)
    }

    fun onDirectionDown(angle: Double) {
        getView()?.enableDirection(angle)
    }

    fun onDirectionUp(view: View, angle: Int) {
        getView()?.let {
            it.disableTouch()
            it.disableDirection()
            when (it.getCurrentAtom().atom.element) {
                Element.MINUS -> {
                    val atom = it.getAtomByAngle(angle)
                    if (atom.atom.element.atom >= Element.PLUS.atom) {
                        it.returnAtom(atom)
                        it.rebalanceAtoms(view, angle.toFloat())
                    }
                }
                Element.MATER -> {
                    val atom = it.getAtomByAngle(angle)
                    it.cloneAtom(atom)
                    it.enableTouch()
                }
                else -> {
                    it.placeAtom(view, angle)
                    it.rebalanceAtoms(view, angle.toFloat())
                }
            }
        }
    }

    fun onRebalanceEnd() {
        getView()?.let {
            val fusionAtoms = it.getFusionAtoms()

            if (fusionAtoms.isNotEmpty()) {
                it.performFusion(fusionAtoms[0])
            } else {
                if (it.atoms.size >= MAX_ATOMS_COUNT) {
                    it.performGameEnd()
                    it.spawnAtom()
                    return
                }

                if (!it.isAfterMinus() || it.atoms.size >= 0) {
                    it.spawnAtom()
                }
                it.enableTouch()
            }
        }
    }

    fun onRemoveEnd() {
        getView()?.enableTouch()
    }

    fun onFusionEnd(atom: AtomView) {
        getView()?.let {
            val fusionAtoms = it.getFusionAtoms(atom)

            if (fusionAtoms.isNotEmpty()) {
                it.performFusion(atom)
            } else {
                it.rebalanceAtoms(atom, (atom.layoutParams as ConstraintLayout.LayoutParams).circleAngle)
            }
        }
    }

    fun onHighestAtomCheck(element: Element) {
        getView()?.let {
            if (it.highestAtom.atom < element.atom) {
                it.highestAtom = element

                val user = dataProvider.getUser()
                if (user.standardModeProgress < it.highestAtom.atom) {
                    user.standardModeProgress = it.highestAtom.atom
                    dataProvider.updateUser(user)
                }
            }
        }
    }

    fun onGameEnd() {
        getView()?.let {
            val user = dataProvider.getUser()

            if (user.standardModeScore < it.currentScore) {
                user.standardModeScore = it.currentScore
                playGamesProvider.submitScore(it as Activity, it.currentScore.toLong(), App.instance.getString(R.string.leaderboard_classic_mode))
            }
            if (user.standardModeProgress < it.highestAtom.atom) {
                user.standardModeProgress = it.highestAtom.atom
            }
            dataProvider.updateUser(user)

            it.showEndGameMenu()
            if (adsProvider.canShowAds() && !remoteConfigProvider.showGameBanner) {
                it.showBanner()
            }
        }
    }

    fun onNewGameClick() {
        getView()?.showAdsIfNeeded(ADS_TYPE_RESTART)
    }

    override fun onAttach(view: V?) {
        super.onAttach(view)
        getView()?.initProviders()
    }

    override fun onDetach() {
        getView()?.let {
            it.destroyBanner()
            it.stopProviders()
        }
        super.onDetach()
    }

    fun onNativeShareClick() {
        getView()?.shareViaIntent()
    }

    fun onStoreClick() {
        getView()?.showStore()
    }

    fun onSpawnAccelerator() {
        dataProvider.changeAcceleratorsCount(-1)

        getView()?.let {
            it.updateUi()
            it.spawnAccelerator()
        }
    }

    fun onAtomStoreClick() {
        getView()?.showAtomStore()
    }

    fun onBackPressed() {
        getView()?.showAdsIfNeeded(ADS_TYPE_HOME)
    }

    fun onPauseClick() {
        getView()?.showPauseDialog()
    }

    fun onSettingsClick() {
        getView()?.showSettings()
    }
}
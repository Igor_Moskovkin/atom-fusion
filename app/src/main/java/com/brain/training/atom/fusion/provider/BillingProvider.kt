package com.brain.training.atom.fusion.provider

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.brain.training.atom.fusion.*
import com.brain.training.atom.fusion.screen.base.view.BaseActivity
import org.solovyev.android.checkout.*
import java.lang.ref.SoftReference

private const val PRO_PACK_ACCELERATORS_COUNT = 5
private const val ONE_DOLLAR_ACCELERATORS_COUNT = 2
private const val TWO_DOLLARS_ACCELERATORS_COUNT = 5
private const val THREE_DOLLARS_ACCELERATORS_COUNT = 10
private const val ONE_DOLLAR_PROTONS_COUNT = 50
private const val TWO_DOLLAR_PROTONS_COUNT = 125
private const val THREE_DOLLAR_PROTONS_COUNT = 250
private const val FIVE_DOLLAR_PROTONS_COUNT = 500

class GoogleBillingProvider(val app: App, val dataProvider: DataProvider) : BillingProvider {
    private val base64encodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvXb3oUQwYf5ynKZdsaPPhNYbltjJjAwhmwAYWDq+uN88AXhZTs8d0M1QFvsOKY4gYl8p6WI6d8vccgs1shiga0+w2HhCxeEYUm6dJRmL/SG4+INpRn5Jzly0ZNvp5LKVYIdqWCJQLD+5Kc6Emcarza1+UHQFpoHzKhXwPGxKa4H8RYvC9IYJ5uu1HfR8XyNhPadxpEsqFeoCpt1FrmYxCdKM5lYDkmlum8c8jlKsHdrQcn0KXHqrIgbRSbyaoFT98ZgpJv3/mfeZW+k+PSQb1dzK1McLPI4ye/Uc3IpsxCTg7r0uRkJRM8jQBYIyB/3CJn/Vpa/8hpW0zvmoRsFSYwIDAQAB"
    override val oneDollarSku = "accelerator_one_dollar"
    override val twoDollarsSku = "accelerator_two_dollars"
    override val threeDollarsSku = "accelerator_three_dollars"
    override val oneDollarParticleSku = "particle_one_dollar"
    override val twoDollarsParticleSku = "particle_two_dollars"
    override val threeDollarsParticleSku = "particle_three_dollars"
    override val fiveDollarsParticleSku = "particle_five_dollars"
    override val removeAdsSku = "remove_ads"
    override val proVersionSku = "pro_version"

    private val inappSkus = listOf(
            oneDollarSku,
            twoDollarsSku,
            threeDollarsSku,
            oneDollarParticleSku,
            twoDollarsParticleSku,
            threeDollarsParticleSku,
            fiveDollarsParticleSku,
            removeAdsSku,
            proVersionSku)
    private val billing = Billing(app, object : Billing.DefaultConfiguration() {
        override fun getPublicKey(): String {
            return base64encodedPublicKey
        }
    })
    private var checkout: ActivityCheckout? = null
//    private var inventory: Inventory? = null
    private val checkouts = mutableMapOf<Activity, SoftReference<ActivityCheckout>>()
    private lateinit var inappList: Inventory.Product
    private lateinit var subsList: Inventory.Product

    override fun setActivity(activity: BaseActivity) {
        checkout = Checkout.forActivity(activity, billing)
        checkout?.start()
        checkout?.createPurchaseFlow(PurchaseListener(SoftReference(activity)))

        reloadInventory(activity)

        checkouts[activity] = SoftReference(checkout!!)
    }

    private fun reloadInventory(activity: BaseActivity) {
        val request = Inventory.Request.create()
        request.loadAllPurchases()
        request.loadSkus(ProductTypes.IN_APP, inappSkus)

        checkout?.makeInventory()?.load(request, InventoryCallback(SoftReference(activity)))
    }

    override fun launchPurchaseFlow(activity: BaseActivity, productType: String, sku: String) {
        App.logEvent(ANALYTICS_START_PURCHASE, Bundle().apply { putString(BUNDLE_KEY_SKU, sku) })

        checkout?.whenReady(object : Checkout.EmptyListener() {
            override fun onReady(requests: BillingRequests) {
                requests.purchase(productType, sku, null, checkout?.purchaseFlow!!)
            }
        })
    }

    override fun onActivityResult(activity: BaseActivity, requestCode: Int, resultCode: Int, data: Intent?) {
        checkout?.onActivityResult(requestCode, resultCode, data)
    }

    override fun destroy(activity: BaseActivity) {
        checkouts[activity]?.get()?.stop()
        checkouts.remove(activity)
        if (checkouts.isEmpty()) {
            checkout?.stop()
            checkout = null
        }
    }

    override fun getSkuProduct(sku: String): Sku? {
        return inappList.getSku(sku)
    }

    private fun consumePurchase(ref: SoftReference<BaseActivity>, purchase: Purchase) {
        checkout?.whenReady(object : Checkout.EmptyListener() {
            override fun onReady(requests: BillingRequests) {
                requests.consume(purchase.token, ConsumeListener(ref, purchase))
            }
        })
    }

    private inner class PurchaseListener(val activityRef: SoftReference<BaseActivity>) : EmptyRequestListener<Purchase>() {

        override fun onSuccess(purchase: Purchase) {
            App.logEvent(ANALYTICS_SUCCESS_PURCHASE, Bundle().apply { putString(BUNDLE_KEY_SKU, purchase.sku) })

            val user = dataProvider.getUser()

            when (purchase.sku) {
                removeAdsSku -> user.adsRemoved = true
                proVersionSku -> {
                    user.isPro = true
                    user.accelerators += PRO_PACK_ACCELERATORS_COUNT
                }
                oneDollarSku,
                twoDollarsSku,
                threeDollarsSku,
                oneDollarParticleSku,
                twoDollarsParticleSku,
                threeDollarsParticleSku,
                fiveDollarsParticleSku -> {
                    consumePurchase(activityRef, purchase)
                }
            }

            dataProvider.updateUser(user)

            activityRef.get()?.updateUi()
        }
    }

    private inner class ConsumeListener(val activityRef: SoftReference<BaseActivity>, val purchase: Purchase) : EmptyRequestListener<Any>() {

        override fun onSuccess(result: Any) {
            App.logEvent(ANALYTICS_CONSUMED_PURCHASE, Bundle().apply { putString(BUNDLE_KEY_SKU, purchase.sku) })

            var accelerators = 0
            var protons = 0

            when (purchase.sku) {
                oneDollarSku -> accelerators = ONE_DOLLAR_ACCELERATORS_COUNT
                twoDollarsSku -> accelerators = TWO_DOLLARS_ACCELERATORS_COUNT
                threeDollarsSku -> accelerators = THREE_DOLLARS_ACCELERATORS_COUNT
                oneDollarParticleSku -> protons = ONE_DOLLAR_PROTONS_COUNT
                twoDollarsParticleSku -> protons = TWO_DOLLAR_PROTONS_COUNT
                threeDollarsParticleSku -> protons = THREE_DOLLAR_PROTONS_COUNT
                fiveDollarsParticleSku -> protons = FIVE_DOLLAR_PROTONS_COUNT
            }

            dataProvider.changeAcceleratorsCount(accelerators)
            dataProvider.changeProtonsCount(protons)

            activityRef.get()?.updateUi()
        }
    }

    private inner class InventoryCallback(val activityRef: SoftReference<BaseActivity>) : Inventory.Callback {

        override fun onLoaded(products: Inventory.Products) {
            inappList = products.get(ProductTypes.IN_APP)
            subsList = products.get(ProductTypes.SUBSCRIPTION)

            var adsRemoved = false
            var isProVersion = false

            for (purchase in inappList.purchases) {
                when (purchase.sku) {
                    removeAdsSku -> adsRemoved = true
                    proVersionSku -> isProVersion = true
                    oneDollarSku,
                    twoDollarsSku,
                    threeDollarsSku,
                    oneDollarParticleSku,
                    twoDollarsParticleSku,
                    threeDollarsParticleSku,
                    fiveDollarsParticleSku -> {
                        consumePurchase(activityRef, purchase)
                    }
                }
            }

            val user = dataProvider.getUser()
            user.adsRemoved = adsRemoved
            user.isPro = isProVersion

            dataProvider.updateUser(user)

//            //consume all products
//            for (product in products.get(ProductTypes.IN_APP).purchases) {
//                checkouts[activityRef.get() as Activity]?.get()?.whenReady(object : Checkout.EmptyListener() {
//                    override fun onReady(requests: BillingRequests) {
//                        requests.consume(product.token, object : EmptyRequestListener<Any>() {
//                            override fun onSuccess(result: Any) {
//                            }
//
//                            override fun onError(response: Int, e: java.lang.Exception) {
//                            }
//                        })
//                    }
//                })
//            }

            activityRef.get()?.updateUi()
        }
    }
}

interface BillingProvider {
    val oneDollarSku: String
    val twoDollarsSku: String
    val threeDollarsSku: String
    val oneDollarParticleSku: String
    val twoDollarsParticleSku: String
    val threeDollarsParticleSku: String
    val fiveDollarsParticleSku: String
    val removeAdsSku: String
    val proVersionSku: String

    fun setActivity(activity: BaseActivity)

    fun launchPurchaseFlow(activity: BaseActivity, productType: String, sku: String)

    fun destroy(activity: BaseActivity)

    fun onActivityResult(activity: BaseActivity, requestCode: Int, resultCode: Int, data: Intent?)

    fun getSkuProduct(sku: String): Sku?

    companion object {
        const val GOOGLE_PLAY_SOURCE = "com.android.vending"
        const val AMAZON_SOURCE = "com.amazon.venezia"
        const val SAMSUNG_SOURCE = "com.sec.android.app.samsungapps"
    }
}
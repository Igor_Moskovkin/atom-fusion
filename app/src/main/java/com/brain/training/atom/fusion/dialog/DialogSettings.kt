package com.brain.training.atom.fusion.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialog
import com.brain.training.atom.fusion.R
import com.brain.training.atom.fusion.provider.NotificationProvider
import com.brain.training.atom.fusion.provider.SoundProvider
import kotlinx.android.synthetic.main.dialog_settings.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class DialogSettings(
        context: Context,
        private val soundProvider: SoundProvider,
        private val notificationProvider: NotificationProvider) : AppCompatDialog(context, R.style.AppTheme_FullScreen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_settings)

        window?.attributes?.dimAmount = 0.6f

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)

        soundSwitch.isChecked = soundProvider.isPlaySound()
        soundSwitch.setOnCheckedChangeListener { _, isChecked ->
            soundProvider.setPlaySound(isChecked)
        }

        notificationSwitch.isChecked = notificationProvider.isShowNotification()
        notificationSwitch.setOnCheckedChangeListener { _, isChecked ->
            notificationProvider.setShowNotification(isChecked)
        }

        okBtn.onClick { dismiss() }

        close.onClick { dismiss() }
    }
}
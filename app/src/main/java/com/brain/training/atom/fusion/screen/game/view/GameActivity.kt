package com.brain.training.atom.fusion.screen.game.view

import android.animation.Animator
import android.animation.ValueAnimator
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.transition.AutoTransition
import androidx.transition.Transition
import androidx.transition.TransitionManager.beginDelayedTransition
import com.brain.training.atom.fusion.*
import com.brain.training.atom.fusion.adapter.AtomStoreListener
import com.brain.training.atom.fusion.di.module.GameModule
import com.brain.training.atom.fusion.dialog.*
import com.brain.training.atom.fusion.model.Atom
import com.brain.training.atom.fusion.model.Element
import com.brain.training.atom.fusion.provider.*
import com.brain.training.atom.fusion.screen.base.view.BaseActivity
import com.brain.training.atom.fusion.screen.game.presenter.GamePresenter
import com.brain.training.atom.fusion.screen.menu.view.MenuActivity
import com.brain.training.atom.fusion.ui.AcceleratorPulseDrawable
import com.brain.training.atom.fusion.ui.AddPulseDrawable
import com.brain.training.atom.fusion.ui.AtomView
import com.brain.training.atom.fusion.ui.RemovePulseDrawable
import com.facebook.share.model.ShareHashtag
import com.facebook.share.model.ShareLinkContent
import com.github.florent37.viewanimator.ViewAnimator
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.activity_game.*
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.textColor
import java.util.*
import javax.inject.Inject


private const val DARK_PLUS_MIN_SCORE = 750
private const val MATER_MIN_SCORE = 1500
internal const val ATOM_SPAWN_DURATION = 200L
private const val PLUS_CHANCE = 6
private const val MINUS_CHANCE = 20
private const val DARK_PLUS_CHANCE = 90
private const val MATER_CHANCE = 60
private const val DEFAULT_TOP_ATOM_RANGE = 2
private const val MOVES_PERIOD = 40
private const val DEFAULT_SCORE_MULTIPLIER = 1.5F
private const val MAX_ATOMS_COUNT = 19
private const val ADS_TYPE_NONE = 0
const val ADS_TYPE_HOME = 1
const val ADS_TYPE_RESTART = 2
private const val PREF_COUNT_BEFORE_ADS = "count_before_ads"

open class GameActivity : BaseActivity(), GameView {
    override val layoutResId: Int = R.layout.activity_game
    override var atoms = mutableListOf<AtomView>()
    override var currentScore: Int = 0
    override var highestAtom: Element = Element.H
        set(value) {
            field = value
            highestAtomProgress.apply {
                text = getString(value.atomName)
                textColor = ContextCompat.getColor(this@GameActivity, value.color)
            }
        }
    private val random: Random by lazy {
        Random(System.currentTimeMillis())
    }
    private val centerX: Int by lazy {
        directionView.width / 2
    }
    private val centerY: Int by lazy {
        directionView.height / 2
    }
    internal val atomSide: Int by lazy {
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        metrics.widthPixels / 8
    }
    internal val atomRadius: Int by lazy {
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        (metrics.widthPixels / 2.5).toInt()
    }
    internal lateinit var spawnedAtom: AtomView
    private var moves: Int = 1
    private var topAtom: Int = DEFAULT_TOP_ATOM_RANGE
        set(value) {
            field = value
            if (value > DEFAULT_TOP_ATOM_RANGE * 3) {
                bottomAtom = value - DEFAULT_TOP_ATOM_RANGE * 2 - moves / 160
            }
        }
    private var bottomAtom: Int = 0
    internal var returnedAtomIndex: Int = 0
    private var acceleratorCounter: Int = 0
    internal var canTouch = false
    private var minusSpawned: Boolean = false
    private var materSpawned: Boolean = false
    private var plusChance: Int = PLUS_CHANCE
    private var currentScoreKoef: Float = DEFAULT_SCORE_MULTIPLIER
    private var tempScore: Int = 0
    private var isPerformingGameEnd: Boolean = false
    private val banner: AdView by lazy {
        adsProvider.getBanner()
    }
    private var dialogStore: DialogStore? = null
    private var pauseDialog: DialogPause? = null
    private var settingsDialog: DialogSettings? = null
    private var atomStoreDialog: DialogAtomStore? = null
    private var selectedAdType: Int = ADS_TYPE_NONE
    private val retryClickListener: OnClickListener = OnClickListener {
        startNewGame()
    }
    private val homeClickListener: OnClickListener = OnClickListener {
        openMainMenu()
    }

    @Inject
    lateinit var presenter: GamePresenter<GameView>
    @Inject
    lateinit var adsProvider: AdsProvider
    @Inject
    lateinit var billingProvider: BillingProvider
    @Inject
    lateinit var dataProvider: DataProvider
    @Inject
    lateinit var remoteConfig: RemoteConfig
    @Inject
    lateinit var playGamesProvider: PlayGamesProvider
    @Inject
    lateinit var soundProvider: SoundProvider
    @Inject
    lateinit var notificationProvider: NotificationProvider

    private var questionDialog: DialogQuestion? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        App.logEvent(ANALYTICS_LEVEL_START, Bundle())

        presenter.onAttach(this)
        presenter.onCreate()

        root.apply {
            setOnTouchListener { _, event ->
                if (canTouch) {
                    when (event.action) {
                        MotionEvent.ACTION_MOVE -> {
                            presenter.onDirectionMove(getAngle(event.x, event.y).toDouble())
                            true
                        }
                        MotionEvent.ACTION_DOWN -> {
                            presenter.onDirectionDown(getAngle(event.x, event.y).toDouble())
                            true
                        }
                        MotionEvent.ACTION_UP -> {
                            presenter.onDirectionUp(spawnedAtom, getAngle(event.x, event.y).toInt())
                            true
                        }
                        else -> true
                    }
                } else {
                    false
                }
            }
        }

        newGameBtn.onClick {
            presenter.onNewGameClick()
        }
        nativeShareBtn.onClick {
            presenter.onNativeShareClick()
        }

        storeBtn.onClick {
            if (canTouch) {
                val accelerators = dataProvider.getAcceleratorsCount()
                when {
                    accelerators > 0 && spawnedAtom.atom.element != Element.ACCELERATOR -> {
                        questionDialog = DialogQuestion(
                                this@GameActivity,
                                R.string.accelerator_use_confirm,
                                OnClickListener {
                                    presenter.onSpawnAccelerator()
                                })
                        questionDialog?.show()
                    }
                    spawnedAtom.atom.element == Element.ACCELERATOR || accelerators <= 0 -> {
                        presenter.onStoreClick()
                    }
                }
            }
        }
        buyAcceleratorBtn.onClick {
            if (canTouch) {
                presenter.onStoreClick()
            }
        }
        atomShopBtn.onClick {
            if (canTouch) {
                presenter.onAtomStoreClick()
            }
        }

        pauseBtn.onClick {
            if (canTouch) {
                presenter.onPauseClick()
            }
        }

        acceleratorIcon.backgroundDrawable = AddPulseDrawable(ContextCompat.getColor(this, R.color.molybdenum))

        score.text = currentScore.toString()

        if (banner.parent != null) {
            (banner.parent as ViewGroup).removeView(banner)
        }
        bannerContainer.addView(banner)
    }

    override fun onStart() {
        super.onStart()
        playGamesProvider.onStart(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
        atomStoreDialog?.updateUi()
    }

    override fun onPause() {
        presenter.onPause()
        super.onPause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        billingProvider.onActivityResult(this, requestCode, resultCode, data)
    }

    override fun onDestroy() {
        presenter.onDetach()

        questionDialog?.dismiss()
        dialogStore?.dismiss()
        atomStoreDialog?.dismiss()
        pauseDialog?.dismiss()
        settingsDialog?.dismiss()

        super.onDestroy()
    }

    override fun performDi() {
        App.instance.component.plus(GameModule()).inject(this)
    }

    override fun spawnStartAtoms() {
        val params = ConstraintLayout.LayoutParams(ConstraintSet.WRAP_CONTENT, ConstraintSet.WRAP_CONTENT).apply {
            bottomToBottom = atomSpawner.id
            topToTop = atomSpawner.id
            startToStart = atomSpawner.id
            endToEnd = atomSpawner.id
        }

        (window.decorView.background as LayerDrawable).apply {
            (findDrawableByLayerId(R.id.circle) as GradientDrawable).apply {
                colors = intArrayOf(ContextCompat.getColor(this@GameActivity, Element.H.color), Color.TRANSPARENT)
                alpha = 45
            }
        }

        val random = Random(System.currentTimeMillis())

        for (i in 0..5) {
            var angle = i * 60 + 10
            if (angle == 360) {
                angle = 0
            }

            val view = AtomView(this, Atom(Element.values()[random.nextInt(3)]), atomSide).apply {
                scaleX = 0F
                scaleY = 0F
                layoutParams = params
                id = generateViewId()
            }
            root.addView(view)

            ViewAnimator.animate(view)
                    .duration(ATOM_SPAWN_DURATION)
                    .scale(0F, 1F)
                    .thenAnimate(view)
                    .duration(ATOM_SPAWN_DURATION / 10)
                    .pulse()
                    .onStop {
                        val circleParams = ConstraintLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
                            circleRadius = 0
                            circleAngle = angle.toFloat()
                            circleConstraint = atomSpawner.id
                        }
                        view.layoutParams = circleParams

                        val animator = ValueAnimator.ofInt(0, atomRadius).apply {
                            duration = ATOM_SPAWN_DURATION / 2
                            interpolator = AccelerateDecelerateInterpolator()
                            addUpdateListener {
                                circleParams.circleRadius = it.animatedValue as Int
                                view.layoutParams = circleParams
                            }
                        }
                        animator.start()

                        atoms.add(view)

                        if (i == 5) {
                            presenter.onHighestAtomCheck(getCurrentHighestAtom())
                        }
                    }
                    .start()
        }
    }

    override fun spawnAtom() {
        val params = ConstraintLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
            bottomToBottom = atomSpawner.id
            topToTop = atomSpawner.id
            startToStart = atomSpawner.id
            endToEnd = atomSpawner.id
        }

        val view = AtomView(this, Atom(getNextAtom()), atomSide).apply {
            scaleX = 0F
            scaleY = 0F
            layoutParams = params
            id = View.generateViewId()
        }
        root.addView(view)

        spawnedAtom = view
        directionView.mainColor = ContextCompat.getColor(this, spawnedAtom.atom.element.color)
        (window.decorView.background as LayerDrawable).apply {
            (findDrawableByLayerId(R.id.circle) as GradientDrawable).apply {
                colors = intArrayOf(ContextCompat.getColor(this@GameActivity, spawnedAtom.atom.element.color), Color.TRANSPARENT)
                alpha = 45
            }
        }

        var pulseView: View? = null
        var pulseSide = 0

        when (spawnedAtom.atom.element) {
            Element.PLUS, Element.DARK_PLUS, Element.MATER -> {
                pulseView = View(this)
                pulseView.backgroundDrawable = AddPulseDrawable(ContextCompat.getColor(this, spawnedAtom.atom.element.color))
                pulseSide = (atomSide * 1.4F).toInt()
            }
            Element.MINUS -> {
                pulseView = View(this)
                pulseView.backgroundDrawable = RemovePulseDrawable(ContextCompat.getColor(this, spawnedAtom.atom.element.color))
                pulseSide = (atomSide * 1.6F).toInt()
            }
            else -> {
            }
        }

        pulseView?.let {
            it.layoutParams = ConstraintLayout.LayoutParams(pulseSide, pulseSide).apply {
                leftToLeft = spawnedAtom.id
                topToTop = spawnedAtom.id
                bottomToBottom = spawnedAtom.id
                rightToRight = spawnedAtom.id
            }
            it.tag = spawnedAtom

            root.addView(it, root.childCount - 2)
        }

        ViewAnimator.animate(view)
                .duration(ATOM_SPAWN_DURATION)
                .scale(0F, 1F)
                .thenAnimate(view)
                .duration(ATOM_SPAWN_DURATION / 10)
                .pulse()
                .start()
    }

    override fun placeAtom(view: View, angle: Int) {
        if (minusSpawned) {
            view.setOnClickListener(null)
            view.isClickable = false
        }
        minusSpawned = false
        materSpawned = false

        val circleParams = ConstraintLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
            circleRadius = atomRadius
            circleAngle = angle.toFloat()
            circleConstraint = atomSpawner.id
        }
        view.layoutParams = circleParams

        atoms.add(view as AtomView)

        moves++
        if (moves % MOVES_PERIOD == 0) {
            topAtom++
        }
    }

    override fun rebalanceAtoms(view: View, angle: Float) {
        presenter.onHighestAtomCheck(getCurrentHighestAtom())

        when (atoms.size) {
            (MAX_ATOMS_COUNT - 1) -> {
                slotDotOne.backgroundResource = R.drawable.turn_slot_circle_red

                slotDotOne.visibility = VISIBLE
                slotDotTwo.visibility = GONE
                slotDotThree.visibility = GONE
            }
            (MAX_ATOMS_COUNT - 2) -> {
                slotDotOne.backgroundResource = R.drawable.turn_slot_circle_yellow
                slotDotTwo.backgroundResource = R.drawable.turn_slot_circle_yellow

                slotDotOne.visibility = VISIBLE
                slotDotTwo.visibility = VISIBLE
                slotDotThree.visibility = GONE
            }
            (MAX_ATOMS_COUNT - 3) -> {
                slotDotOne.backgroundResource = R.drawable.turn_slot_circle_green
                slotDotTwo.backgroundResource = R.drawable.turn_slot_circle_green
                slotDotThree.backgroundResource = R.drawable.turn_slot_circle_green

                slotDotOne.visibility = VISIBLE
                slotDotTwo.visibility = VISIBLE
                slotDotThree.visibility = VISIBLE
            }
            else -> {
                slotDotOne.visibility = GONE
                slotDotTwo.visibility = GONE
                slotDotThree.visibility = GONE
            }
        }

        if (atoms.size <= 1) {
            presenter.onRebalanceEnd()
            return
        }

        atoms = atoms.sortedBy {
            (it.layoutParams as ConstraintLayout.LayoutParams).circleAngle
        }.toMutableList()

        val sector: Float = 360F / atoms.size

        beginDelayedTransition(root, AutoTransition().apply {
            interpolator = AccelerateDecelerateInterpolator()
            duration = 100
            addListener(object : Transition.TransitionListener {
                override fun onTransitionEnd(transition: Transition) {
                    currentScoreKoef = DEFAULT_SCORE_MULTIPLIER

                    presenter.onRebalanceEnd()
                }

                override fun onTransitionResume(transition: Transition) {
                }

                override fun onTransitionPause(transition: Transition) {
                }

                override fun onTransitionCancel(transition: Transition) {
                }

                override fun onTransitionStart(transition: Transition) {
                }
            })
        })

        val atomIndex = atoms.indexOf(view)

        if (atomIndex == -1) {
            for (i in returnedAtomIndex until atoms.size) {
                (atoms[i].layoutParams as ConstraintLayout.LayoutParams).circleAngle = if (i * sector > 360) {
                    i * sector - 360
                } else {
                    i * sector
                }
            }
            for (i in (returnedAtomIndex - 1) downTo 0) {
                (atoms[i].layoutParams as ConstraintLayout.LayoutParams).circleAngle = if (i * sector < 0) {
                    i * sector + 360
                } else {
                    i * sector
                }
            }
        } else {
            var targetDownAngle = angle
            var targetUpAngle = angle

            when (atomIndex) {
                0 -> {
                    for (i in (atoms.indexOf(view) + 1) until atoms.size) {
                        targetUpAngle += sector

                        if (targetUpAngle > 360) {
                            targetUpAngle -= 360
                        }

                        val params = atoms[i].layoutParams as ConstraintLayout.LayoutParams
                        params.circleAngle = targetUpAngle
                        atoms[i].layoutParams = params
                    }
                }
                (atoms.size - 1) -> {
                    for (i in atoms.indexOf(view) - 1 downTo 0) {
                        targetDownAngle -= sector

                        if (targetDownAngle < 0) {
                            targetDownAngle += 360
                        }

                        val params = atoms[i].layoutParams as ConstraintLayout.LayoutParams
                        params.circleAngle = targetDownAngle
                        atoms[i].layoutParams = params
                    }
                }
                else -> {
                    for (i in atoms.indexOf(view) - 1 downTo 0) {
                        targetDownAngle -= sector

                        if (targetDownAngle < 0) {
                            targetDownAngle += 360
                        }

                        val params = atoms[i].layoutParams as ConstraintLayout.LayoutParams
                        params.circleAngle = targetDownAngle
                        atoms[i].layoutParams = params
                    }
                    for (i in (atoms.indexOf(view) + 1) until atoms.size) {
                        targetUpAngle += sector

                        if (targetUpAngle > 360) {
                            targetUpAngle -= 360
                        }

                        val params = atoms[i].layoutParams as ConstraintLayout.LayoutParams
                        params.circleAngle = targetUpAngle
                        atoms[i].layoutParams = params
                    }
                }
            }
        }
    }

    override fun getCurrentHighestAtom(): Element {
        if (atoms.isEmpty()) {
            return highestAtom
        }

        return Collections.max(atoms) { o1, o2 ->
            o1.atom.element.atom - o2.atom.element.atom
        }.atom.element
    }

    private fun getCurrentLowestAtom(): AtomView {
        return Collections.min(atoms) { o1, o2 ->
            when {
                o1.atom.element.atom < 0 && o2.atom.element.atom > 0 -> 1
                o1.atom.element.atom > 0 && o2.atom.element.atom < 0 -> -1
                else -> o1.atom.element.atom - o2.atom.element.atom
            }
        }
    }

    open fun getNextAtom(): Element {
        if (moves % MINUS_CHANCE == 0) {
            minusSpawned = true
            return Element.MINUS
        }

        if (currentScore >= DARK_PLUS_MIN_SCORE && random.nextInt(DARK_PLUS_CHANCE) == 0) {
            acceleratorCounter++
            return Element.DARK_PLUS
        }

        if (currentScore >= MATER_MIN_SCORE && random.nextInt(MATER_CHANCE) == 0) {
            materSpawned = true
            return Element.MATER
        }

        val spawnPlusAtom = random.nextInt(plusChance)

        return if (spawnPlusAtom == 0) {
            plusChance = PLUS_CHANCE
            Element.PLUS
        } else {
            plusChance--

            if (atoms.size == 0) {
                return Element.values()[random.nextInt((topAtom - bottomAtom) + 1) + bottomAtom]
            }

            val minAtom = getCurrentLowestAtom()

            if (minAtom.atom.element.atom < bottomAtom && atoms.size > 0) {
                val lowAtomChance = random.nextInt(atoms.size)

                if (lowAtomChance == 0) {
                    val index = random.nextInt((bottomAtom - minAtom.atom.element.atom) + 1) + minAtom.atom.element.atom
                    Element.values()[index]
                } else {
                    Element.values()[random.nextInt((topAtom - bottomAtom) + 1) + bottomAtom]
                }
            } else {
                Element.values()[random.nextInt((topAtom - bottomAtom) + 1) + bottomAtom]
            }
        }
    }

    private fun getAngle(x: Float, y: Float): Float {
        var angle = Math.toDegrees(Math.atan2((y - centerY).toDouble(), (x - centerX).toDouble())).toFloat() + 90

        if (angle < 0) {
            angle += 360
        }

        var switch = false
        val halfSector = (360F / atoms.size) / 2
        val downLimit = if (angle - halfSector < 0) {
            switch = true
            angle - halfSector + 360
        } else {
            angle - halfSector
        }
        val upLimit = if (angle + halfSector > 360) {
            switch = true
            angle + halfSector - 360
        } else {
            angle + halfSector
        }

        val atom = atoms.firstOrNull {
            val params = it.layoutParams as ConstraintLayout.LayoutParams
            if (switch) {
                params.circleAngle in downLimit..359.99F || params.circleAngle in 0F..upLimit
            } else {
                params.circleAngle in downLimit..upLimit
            }
        }
                ?: return (atoms[0].layoutParams as ConstraintLayout.LayoutParams).circleAngle - halfSector

        val params = atom.layoutParams as ConstraintLayout.LayoutParams

        if (spawnedAtom.atom.element.atom == Element.MINUS.atom) {
            return params.circleAngle
        }

        if (spawnedAtom.atom.element.atom == Element.MATER.atom) {
            return params.circleAngle
        }

        return when {
            atoms.size == 0 -> 0F
            params.circleAngle - halfSector < 0 -> {
                if (angle in params.circleAngle..params.circleAngle + halfSector) {
                    params.circleAngle + halfSector
                } else {
                    params.circleAngle - halfSector + 360
                }
            }
            params.circleAngle + halfSector > 360 -> {
                if (angle in params.circleAngle - halfSector..params.circleAngle) {
                    params.circleAngle - halfSector
                } else {
                    params.circleAngle + halfSector - 360
                }
            }
            else -> if (params.circleAngle > angle) params.circleAngle - halfSector else params.circleAngle + halfSector
        }
    }

    override fun enableDirection(currentAngle: Double) {
        directionView.apply {
            drawDirection = true
            angle = currentAngle
        }
    }

    override fun disableDirection() {
        directionView.apply {
            drawDirection = false
            angle = 0.0
        }
    }

    override fun setDirection(angle: Double) {
        directionView.angle = angle
    }

    override fun getFusionAtoms(): List<AtomView> {
        if (atoms.size < 3) {
            return listOf()
        }

        return atoms.filter {
            var result = false

            when (it.atom.element) {
                Element.PLUS -> when {
                    atoms.indexOf(it) == 0 -> {
                        if (atoms[1].atom == atoms[atoms.size - 1].atom && atoms[1].atom.element.atom > 0) {
                            result = true
                        }
                    }
                    atoms.indexOf(it) == (atoms.size - 1) -> {
                        if (atoms[atoms.size - 2].atom == atoms[0].atom && atoms[0].atom.element.atom > 0) {
                            result = true
                        }
                    }
                    else -> {
                        if (atoms[atoms.indexOf(it) - 1].atom == atoms[atoms.indexOf(it) + 1].atom && atoms[atoms.indexOf(it) + 1].atom.element.atom > 0) {
                            result = true
                        }
                    }
                }
                Element.ACCELERATOR, Element.DARK_PLUS -> result = true
                else -> result = false
            }
            result
        }
    }

    override fun getFusionAtoms(atom: AtomView): List<AtomView> {
        if (atoms.size < 3) {
            return listOf()
        }

        val prevAtom = getPrevAtom(atom)
        val nextAtom = getNextAtom(atom)

        if (acceleratorCounter > 0) return listOf(atom)

        if (prevAtom.atom.element.atom < 1 || nextAtom.atom.element.atom < 1) return listOf()

        return if (prevAtom.atom.element.atom == nextAtom.atom.element.atom) {
            listOf(atom)
        } else {
            listOf()
        }
    }

    override fun performFusion(atom: AtomView) {
        if (acceleratorCounter > 0) acceleratorCounter--

        val angle = (atom.layoutParams as ConstraintLayout.LayoutParams).circleAngle
        val prevAtom = getPrevAtom(atom)
        val nextAtom = getNextAtom(atom)

        val prevParams = prevAtom.layoutParams as ConstraintLayout.LayoutParams
        val nextParams = nextAtom.layoutParams as ConstraintLayout.LayoutParams

        val prevStartAngle: Float = if (prevParams.circleAngle + Math.abs(angle - prevParams.circleAngle) > 360) {
            prevParams.circleAngle - 360
        } else {
            prevParams.circleAngle
        }

        val nextStartAngle: Float = if (nextParams.circleAngle - Math.abs(angle - nextParams.circleAngle) < 0) {
            nextParams.circleAngle + 360
        } else {
            nextParams.circleAngle
        }

        val prevAnimator = ValueAnimator.ofFloat(prevStartAngle, angle).apply {
            duration = 300
            addUpdateListener {
                prevParams.circleAngle = it.animatedValue as Float
                prevAtom.layoutParams = prevParams
            }
        }
        prevAnimator.start()

        val nextAnimator = ValueAnimator.ofFloat(nextStartAngle, angle).apply {
            duration = 300
            addUpdateListener {
                nextParams.circleAngle = it.animatedValue as Float
                nextAtom.layoutParams = nextParams
            }
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {
                }

                override fun onAnimationEnd(animation: Animator?) {
                    val prevElement = when (prevAtom.atom.element) {
                        Element.PLUS -> Element.H
                        else -> prevAtom.atom.element
                    }
                    val nextElement = when (nextAtom.atom.element) {
                        Element.PLUS -> Element.H
                        else -> nextAtom.atom.element
                    }
                    root.findViewWithTag<View?>(prevAtom)?.let { root.removeView(it) }
                    root.findViewWithTag<View?>(nextAtom)?.let { root.removeView(it) }

                    atoms.remove(prevAtom)
                    atoms.remove(nextAtom)

                    root.removeView(prevAtom)
                    root.removeView(nextAtom)

                    val elementBeforeFusion = atom.atom

                    atom.atom = when {
                        atom.atom.element == Element.PLUS -> {
                            Atom(Element.values()[prevElement.ordinal + 1])
                        }
                        atom.atom.element == Element.DARK_PLUS -> {
                            if (prevElement == Element.PLUS && nextElement == Element.PLUS) {
                                Atom(Element.BE)
                            } else {
                                if (prevElement.atom > nextElement.atom) {
                                    Atom(Element.values()[prevElement.ordinal + 3])
                                } else {
                                    Atom(Element.values()[nextElement.ordinal + 3])
                                }
                            }
                        }
                        prevElement.atom < atom.atom.element.atom && nextElement.atom < atom.atom.element.atom -> {
                            Atom(Element.values()[atom.atom.element.ordinal + 1])
                        }
                        else -> {
                            Atom(Element.values()[prevElement.ordinal + 2])
                        }
                    }

                    ViewAnimator.animate(atom)
                            .duration(50)
                            .scale(1F, 1.3F)
                            .thenAnimate(atom)
                            .duration(50)
                            .scale(1.3F, 0.9F)
                            .thenAnimate(atom)
                            .duration(50)
                            .scale(0.9F, 1F)
                            .thenAnimate(atom)
                            .duration(100)
                            .pulse()
                            .start()

                    if (elementBeforeFusion.element.atom < 0) {
                        root.removeView(root.findViewWithTag(atom))
                    }

//                    performFusionAnimation(atom)

                    updateScore(prevAtom.atom, atom.atom, elementBeforeFusion)

                    presenter.onFusionEnd(atom)
                }

                override fun onAnimationCancel(animation: Animator?) {
                }

                override fun onAnimationStart(animation: Animator?) {
                }
            })
        }
        nextAnimator.start()
    }

//    private fun performFusionAnimation(atom: AtomView) {
//        val params = ConstraintLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
//            bottomToBottom = atomSpawner.id
//            topToTop = atomSpawner.id
//            startToStart = atomSpawner.id
//            endToEnd = atomSpawner.id
//        }
//
//        val view = AtomView(this, atom.atom, atomRadius).apply {
//            alpha = 0.2F
//            layoutParams = params
//        }
//        root.addView(view)
//
//        ViewAnimator.animate(view)
//                .duration(500)
//                .scale(0F, 0.6F)
//                .onStop { root.removeView(view) }
//                .start()
//    }

    private fun updateScore(prevAtom: Atom, fusedAtom: Atom, beforeFusionAtom: Atom) {
        tempScore = (currentScoreKoef * (prevAtom.element.atom + 1)).toInt()

        if (prevAtom.element.atom > fusedAtom.element.atom) {
            tempScore += (currentScoreKoef * 2 * (prevAtom.element.atom - beforeFusionAtom.element.atom + 1)).toInt()
        }

        currentScore += tempScore
        tempScore = 0
        currentScoreKoef += 0.5F

        ViewAnimator.animate(score)
                .duration(80)
                .scale(1F, 1.2F)
                .thenAnimate(score)
                .duration(80)
                .scale(1.2F, 1F)
                .onStart { score.text = currentScore.toString() }
                .start()
    }

    private fun getPrevAtom(atom: AtomView): AtomView {
        return when {
            atoms.indexOf(atom) == 0 -> {
                atoms[atoms.size - 1]
            }
            atoms.indexOf(atom) == (atoms.size - 1) -> {
                atoms[atoms.size - 2]
            }
            else -> {
                atoms[atoms.indexOf(atom) - 1]
            }
        }
    }

    private fun getNextAtom(atom: AtomView): AtomView {
        return when {
            atoms.indexOf(atom) == (atoms.size - 1) -> {
                atoms[0]
            }
            else -> {
                atoms[atoms.indexOf(atom) + 1]
            }
        }
    }

    override fun enableTouch() {
        canTouch = true
    }

    override fun disableTouch() {
        canTouch = false
    }

    override fun getCurrentAtom(): AtomView {
        return spawnedAtom
    }

    override fun getAtomByAngle(angle: Int): AtomView {
        val atomView: AtomView? = atoms.firstOrNull {
            (it.layoutParams as ConstraintLayout.LayoutParams).circleAngle.toInt() == angle
        }

        return atomView ?: atoms[0]
    }

    override fun returnAtom(atom: AtomView) {
        returnedAtomIndex = atoms.indexOf(atom)

        val params = ConstraintLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
            bottomToBottom = atomSpawner.id
            topToTop = atomSpawner.id
            startToStart = atomSpawner.id
            endToEnd = atomSpawner.id
        }

        if (spawnedAtom.atom.element.atom < 0) {
            root.removeView(root.findViewWithTag(spawnedAtom))
        }
        root.removeView(spawnedAtom)
        spawnedAtom = atom
        atoms.remove(atom)
        directionView.mainColor = ContextCompat.getColor(this, spawnedAtom.atom.element.color)

        beginDelayedTransition(root, AutoTransition().apply {
            interpolator = AccelerateDecelerateInterpolator()
            duration = 100
            addListener(object : Transition.TransitionListener {
                override fun onTransitionEnd(transition: Transition) {
                    presenter.onRemoveEnd()
                }

                override fun onTransitionResume(transition: Transition) {
                }

                override fun onTransitionPause(transition: Transition) {
                }

                override fun onTransitionCancel(transition: Transition) {
                }

                override fun onTransitionStart(transition: Transition) {
                }

            })
        })
        spawnedAtom.apply {
            layoutParams = params
            setOnClickListener {
                ViewAnimator.animate(spawnedAtom)
                        .duration(150)
                        .scale(1F, 0F)
                        .onStop {
                            spawnedAtom.atom = Atom(Element.PLUS)
                            val pulseView = View(this@GameActivity).apply {
                                backgroundDrawable = AddPulseDrawable(ContextCompat.getColor(this@GameActivity, spawnedAtom.atom.element.color))

                                layoutParams = ConstraintLayout.LayoutParams((atomSide * 1.4F).toInt(), (atomSide * 1.4F).toInt()).apply {
                                    leftToLeft = spawnedAtom.id
                                    topToTop = spawnedAtom.id
                                    bottomToBottom = spawnedAtom.id
                                    rightToRight = spawnedAtom.id
                                }

                                tag = spawnedAtom
                            }

                            root.addView(pulseView, root.childCount - 2)
                        }
                        .thenAnimate(spawnedAtom)
                        .duration(150)
                        .scale(0F, 1F)
                        .thenAnimate(spawnedAtom)
                        .duration(100)
                        .pulse()
                        .start()
                it?.setOnClickListener(null)
                it?.isClickable = false
            }
        }
    }

    override fun isAfterMinus(): Boolean {
        return minusSpawned
    }

    override fun performGameEnd() {
        if (atoms.isEmpty()) {
            presenter.onGameEnd()
            return
        }

        val atom = atoms[random.nextInt(atoms.size)]

        if (atom.atom.element.atom < 0) {
            root.removeView(root.findViewWithTag(atom))
        }

        val params = ConstraintLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
            bottomToBottom = atomSpawner.id
            topToTop = atomSpawner.id
            startToStart = atomSpawner.id
            endToEnd = atomSpawner.id
        }

        beginDelayedTransition(root, AutoTransition().apply {
            interpolator = AccelerateDecelerateInterpolator()
            duration = 200
            addListener(object : Transition.TransitionListener {
                override fun onTransitionEnd(transition: Transition) {
                    if (atoms.size > 0) {
                        if (spawnedAtom.atom.element.atom < atom.atom.element.atom) {
                            spawnedAtom.atom = atom.atom
                            (window.decorView.background as LayerDrawable).apply {
                                (findDrawableByLayerId(R.id.circle) as GradientDrawable).apply {
                                    colors = intArrayOf(ContextCompat.getColor(this@GameActivity, spawnedAtom.atom.element.color), Color.TRANSPARENT)
                                    alpha = 45
                                }
                            }
                        }

                        ViewAnimator.animate(spawnedAtom)
                                .duration(100)
                                .pulse()
                                .start()

                        root.removeView(atom)
                        atoms.remove(atom)
                        directionView.mainColor = ContextCompat.getColor(this@GameActivity, spawnedAtom.atom.element.color)

                        currentScore += if (atom.atom.element.atom < 0) {
                            atom.atom.element.atom + 10
                        } else {
                            atom.atom.element.atom
                        }

                        ViewAnimator.animate(score)
                                .duration(80)
                                .scale(1F, 1.2F)
                                .thenAnimate(score)
                                .duration(80)
                                .scale(1.2F, 1F)
                                .onStart { score.text = currentScore.toString() }
                                .start()

                        performGameEnd()
                    } else {
                        presenter.onGameEnd()
                    }
                }

                override fun onTransitionResume(transition: Transition) {
                }

                override fun onTransitionPause(transition: Transition) {
                }

                override fun onTransitionCancel(transition: Transition) {
                }

                override fun onTransitionStart(transition: Transition) {
                }

            })
        })

        if (!isPerformingGameEnd) {
            isPerformingGameEnd = true

            root.removeView(root.findViewWithTag(spawnedAtom))

            ViewAnimator.animate(directionView)
                    .duration(4500)
                    .scale(1F, 0.2F)
                    .start()

            ViewAnimator.animate(score)
                    .duration(4500)
                    .translationY(centerY.toFloat() / 5F)
                    .thenAnimate(score)
                    .duration(100)
                    .translationY(centerY / 1.65F)
                    .start()

            ViewAnimator.animate(highestAtomProgress)
                    .duration(4500)
                    .translationY(centerY.toFloat() / 5F)
                    .thenAnimate(highestAtomProgress)
                    .duration(100)
                    .translationY(centerY / 1.65F)
                    .start()
        }

        atom.layoutParams = params
    }

    override fun openMainMenu() {
        startActivity(intentFor<MenuActivity>())
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        finish()
    }

    override fun startNewGame() {
        startActivity(intentFor<GameActivity>())
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        finish()
    }

    override fun showEndGameMenu() {
        App.logEvent(ANALYTICS_LEVEL_END, Bundle())

        fbShareBtn.apply {
            shareContent = ShareLinkContent.Builder()
                    .setQuote(String.format(getString(R.string.share_text), currentScore, getString(highestAtom.atomName)))
                    .setShareHashtag(ShareHashtag.Builder().apply { hashtag = "#AtomFusionGame" }.build())
                    .setContentUrl(Uri.parse("$WEB_APP_URL&referrer=utm_source%3DFacebookShare"))
                    .build()
        }

        ViewAnimator.animate(newGameBtn, nativeShareBtn)
                .duration(300)
                .onStart {
                    newGameBtn.visibility = VISIBLE
                    nativeShareBtn.visibility = VISIBLE
                    fbShareBtn.visibility = VISIBLE
                }
                .fadeIn()
                .start()
    }

    override fun showBanner() {
        bannerContainer.visibility = VISIBLE
    }

    override fun hideBanner() {
        bannerContainer.visibility = GONE
    }

    override fun shareViaIntent() {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND

        intent.type = "text/plain"
        intent.putExtra(
                Intent.EXTRA_TEXT,
                String.format(getString(R.string.share_text), currentScore, getString(highestAtom.atomName)) + "https://goo.gl/8tYpzH")
        startActivity(Intent.createChooser(intent, getString(R.string.share_button)))
    }

    override fun pauseBanner() {
        banner.pause()
    }

    override fun resumeBanner() {
        banner.resume()
    }

    override fun destroyBanner() {
        bannerContainer.removeAllViews()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    override fun initProviders() {
        billingProvider.setActivity(this)
        adsProvider.subscribe(this)
    }

    override fun stopProviders() {
        billingProvider.destroy(this)
        adsProvider.unsubscribe(this)
    }

    override fun updateUi() {
        dialogStore?.updateUi()
        atomStoreDialog?.updateUi()
        acceleratorsCount.text = dataProvider.getAcceleratorsCount().toString()
    }

    override fun showStore() {
        dialogStore = DialogStore(this, billingProvider, dataProvider, DialogStore.StoreItemType.ACCELERATOR)
        dialogStore?.show()
    }

    override fun hideStore() {
        dialogStore?.dismiss()
    }

    override fun onAdClosed() {
        val onClickListener: View.OnClickListener? = when (selectedAdType) {
            ADS_TYPE_RESTART -> retryClickListener
            ADS_TYPE_HOME -> homeClickListener
            else -> null
        }

        selectedAdType = ADS_TYPE_NONE
        onClickListener?.onClick(null)
    }

    override fun showAdsIfNeeded(type: Int) {
        selectedAdType = ADS_TYPE_NONE

        val onClickListener: View.OnClickListener = when (type) {
            ADS_TYPE_RESTART -> retryClickListener
            ADS_TYPE_HOME -> homeClickListener
            else -> return
        }

        if (!adsProvider.canShowAds()) {
            onClickListener.onClick(null)
            return
        }

        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this@GameActivity)

        var countBeforeAds = sharedPreferences.getInt(PREF_COUNT_BEFORE_ADS, remoteConfig.interstitialInterval)
        countBeforeAds--

        if (countBeforeAds <= 0) {
            selectedAdType = type

            adsProvider.showInterstitial()

            countBeforeAds = remoteConfig.interstitialInterval
        } else {
            onClickListener.onClick(null)
        }
        sharedPreferences.edit().putInt(PREF_COUNT_BEFORE_ADS, countBeforeAds).apply()
    }

    override fun spawnAccelerator() {
        App.logEvent(ANALYTICS_ACCELERATOR_USED, Bundle())

        val totalAtomsCount = atoms.size + 1

        acceleratorCounter = when {
            totalAtomsCount <= 4 -> 2
            totalAtomsCount <= 8 -> 3
            totalAtomsCount <= 12 -> 4
            totalAtomsCount <= 16 -> 5
            totalAtomsCount <= 20 -> 6
            else -> totalAtomsCount / 4
        }

        spawnedAtom.atom = Atom(Element.ACCELERATOR)

        var pulseView = root.findViewWithTag<View?>(spawnedAtom)

        if (pulseView == null) {
            pulseView = View(this).apply {
                tag = spawnedAtom
            }
            root.addView(pulseView, root.indexOfChild(spawnedAtom) - 2)
        }

        pulseView.apply {
            backgroundDrawable = AcceleratorPulseDrawable(ContextCompat.getColor(this@GameActivity, spawnedAtom.atom.element.color))
            layoutParams = ConstraintLayout.LayoutParams((atomSide * 2F).toInt(), (atomSide * 2F).toInt()).apply {
                leftToLeft = spawnedAtom.id
                topToTop = spawnedAtom.id
                bottomToBottom = spawnedAtom.id
                rightToRight = spawnedAtom.id
            }
        }
    }

    override fun cloneAtom(atom: AtomView) {
        ViewAnimator.animate(spawnedAtom)
                .duration(300)
                .scale(0F, 1F)
                .onStart { spawnedAtom.atom = atom.atom }
                .thenAnimate(spawnedAtom)
                .duration(100)
                .pulse()
                .start()

        when (atom.atom.element) {
            Element.PLUS, Element.DARK_PLUS, Element.ACCELERATOR -> {
                var pulseView = root.findViewWithTag<View?>(spawnedAtom)

                if (pulseView == null) {
                    pulseView = View(this).apply {
                        tag = spawnedAtom
                    }
                    root.addView(pulseView, root.indexOfChild(spawnedAtom) - 2)
                }

                pulseView.apply {
                    backgroundDrawable = AcceleratorPulseDrawable(ContextCompat.getColor(this@GameActivity, spawnedAtom.atom.element.color))
                    layoutParams = ConstraintLayout.LayoutParams((atomSide * 1.4F).toInt(), (atomSide * 1.4F).toInt()).apply {
                        leftToLeft = spawnedAtom.id
                        topToTop = spawnedAtom.id
                        bottomToBottom = spawnedAtom.id
                        rightToRight = spawnedAtom.id
                    }
                }
            }
            else -> {
                root.findViewWithTag<View?>(spawnedAtom)?.let {
                    root.removeView(it)
                }

            }
        }
    }

    override fun showAtomStore() {
        atomStoreDialog = DialogAtomStore(this, dataProvider, remoteConfig, adsProvider, playGamesProvider, billingProvider, atomSide, highestAtom, object : AtomStoreListener {
            override fun onAtomClick(element: Element) {
                playGamesProvider.submitSpendProtons(this@GameActivity, element.atom)
                dataProvider.changeProtonsCount(-element.atom)

                root.findViewWithTag<View?>(spawnedAtom)?.let {
                    root.removeView(it)
                }

                ViewAnimator.animate(spawnedAtom)
                        .duration(300)
                        .scale(0F, 1F)
                        .onStart {
                            spawnedAtom.atom = Atom(element)
                            directionView.mainColor = ContextCompat.getColor(this@GameActivity, spawnedAtom.atom.element.color)
                            (window.decorView.background as LayerDrawable).apply {
                                (findDrawableByLayerId(R.id.circle) as GradientDrawable).apply {
                                    colors = intArrayOf(ContextCompat.getColor(this@GameActivity, spawnedAtom.atom.element.color), Color.TRANSPARENT)
                                    alpha = 45
                                }
                            }
                        }
                        .thenAnimate(spawnedAtom)
                        .duration(100)
                        .pulse()
                        .start()
                atomStoreDialog?.dismiss()
            }
        })
        atomStoreDialog?.show()
    }

    override fun showPauseDialog() {
        if (pauseDialog != null && pauseDialog?.isShowing!!) {
            return
        }

        pauseDialog = DialogPause(
                this,
                OnClickListener { presenter.onNewGameClick() },
                OnClickListener { presenter.onBackPressed() },
                OnClickListener { presenter.onSettingsClick() })
        pauseDialog?.show()
    }

    override fun showSettings() {
        settingsDialog = DialogSettings(this, soundProvider, notificationProvider)
        settingsDialog?.show()
    }
}
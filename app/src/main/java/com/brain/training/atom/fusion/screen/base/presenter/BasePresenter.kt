package com.brain.training.atom.fusion.screen.base.presenter

import com.brain.training.atom.fusion.screen.base.view.BaseView

abstract class BasePresenter<V : BaseView> : Presenter<V> {
    private var view: V? = null
    private val isViewAttached: Boolean get() = view != null

    override fun onAttach(view: V?) {
        this.view = view
    }

    override fun getView(): V? = view

    override fun onDetach() {
        view = null
    }
}
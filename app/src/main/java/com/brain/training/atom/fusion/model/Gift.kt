package com.brain.training.atom.fusion.model

data class Gift(var giftType: Int, var protons: Int, var accelerators: Int)
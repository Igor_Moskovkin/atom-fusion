package com.brain.training.atom.fusion.screen.menu.view

import com.brain.training.atom.fusion.screen.base.view.BaseView

interface MenuView : BaseView{

    fun startNormalGame()

    fun setHighestScore(score: String)

    fun showLeaderboard()

    fun showLoadingDialog()

    fun hideLoadingDialog()

    fun openSettings()

    fun startGiftsTimer()

    fun startTutorial()
}
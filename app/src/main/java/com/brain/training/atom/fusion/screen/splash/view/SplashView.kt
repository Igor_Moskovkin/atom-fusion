package com.brain.training.atom.fusion.screen.splash.view

import com.brain.training.atom.fusion.screen.base.view.BaseView

interface SplashView : BaseView {

    fun openMainMenu()

    fun checkForGift()
}
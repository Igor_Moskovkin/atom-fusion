package com.brain.training.atom.fusion.provider

import com.brain.training.atom.fusion.provider.database.AppDatabase
import com.brain.training.atom.fusion.provider.database.model.User

class DataProviderImpl(private val database: AppDatabase) : DataProvider {

    init {
        if (database.userDao().getUsersCount() == 0) {
            database.userDao().insertUser(User())
        }
    }

    override fun getUser(): User {
        return database.userDao().getUser()
    }

    override fun updateUser(user: User) {
        database.userDao().updateUser(user)
    }

    override fun getAcceleratorsCount(): Int {
        return getUser().accelerators
    }

    override fun getProtonsCount(): Int {
        return getUser().protons
    }

    override fun changeAcceleratorsCount(i: Int) {
        database.userDao().updateUser(getUser().apply { accelerators += i })
    }

    override fun changeProtonsCount(i: Int) {
        database.userDao().updateUser(getUser().apply { protons += i })
    }
}

interface DataProvider {

    fun getUser(): User

    fun updateUser(user: User)

    fun getAcceleratorsCount(): Int

    fun getProtonsCount(): Int

    fun changeAcceleratorsCount(i: Int)

    fun changeProtonsCount(i: Int)
}
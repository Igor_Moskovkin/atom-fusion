package com.brain.training.atom.fusion.model

import androidx.annotation.DrawableRes

data class StoreItem(val sku: String, val productType: String, val title: String, val price: String, @DrawableRes val iconRes: Int)
package com.brain.training.atom.fusion.receiver

import android.app.Notification
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import com.brain.training.atom.fusion.ANALYTICS_GIFT_NOTIFICATION_SHOWN
import com.brain.training.atom.fusion.ANALYTICS_REMINDER_NOTIFICATION_SHOWN
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.provider.NotificationProvider
import com.brain.training.atom.fusion.provider.NotificationProvider.Companion.GIFT_NOTIFICATION_ID
import com.brain.training.atom.fusion.provider.NotificationProvider.Companion.REMINDER_NOTIFICATION_ID
import com.brain.training.atom.fusion.provider.NotificationProvider.Companion.REMINDER_NOTIFICATION_SHOWN
import com.brain.training.atom.fusion.screen.base.view.BaseActivity
import javax.inject.Inject


class NotificationReceiver : BroadcastReceiver() {
    @Inject
    lateinit var notificationProvider: NotificationProvider

    override fun onReceive(context: Context, intent: Intent) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)

        if (!prefs.getBoolean(NotificationProvider.SHOW_NOTIFICATION_KEY, true)) {
            return
        }

        App.instance.component.inject(this)

        if (!BaseActivity.isInForeground) {
            val id = intent.getIntExtra(NOTIFICATION_ID, 0)

            when (id) {
                GIFT_NOTIFICATION_ID -> {
                    val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    val notification = intent.getParcelableExtra<Notification>(NOTIFICATION)

                    notificationManager.notify(id, notification)

                    App.logEvent(ANALYTICS_GIFT_NOTIFICATION_SHOWN, Bundle())
                }
                REMINDER_NOTIFICATION_ID -> {
                    val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    val notification = intent.getParcelableExtra<Notification>(NOTIFICATION)

                    notificationManager.notify(id, notification)

                    var counter = prefs.getInt(REMINDER_NOTIFICATION_SHOWN, 0)
                    counter++

                    prefs.edit().putInt(REMINDER_NOTIFICATION_SHOWN, counter).apply()

                    notificationProvider.scheduleRemindNotification()

                    App.logEvent(ANALYTICS_REMINDER_NOTIFICATION_SHOWN, Bundle())
                }
            }
        }
    }

    companion object {
        const val NOTIFICATION_ID = "notification_id"
        const val NOTIFICATION = "notification"
    }
}
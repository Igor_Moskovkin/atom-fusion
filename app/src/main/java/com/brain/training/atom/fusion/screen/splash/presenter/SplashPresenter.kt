package com.brain.training.atom.fusion.screen.splash.presenter

import com.brain.training.atom.fusion.screen.base.presenter.BasePresenter
import com.brain.training.atom.fusion.screen.splash.view.SplashView

class SplashPresenter<V : SplashView> : BasePresenter<V>() {

    override fun onAttach(view: V?) {
        super.onAttach(view)
        view?.checkForGift()
        view?.openMainMenu()
    }
}
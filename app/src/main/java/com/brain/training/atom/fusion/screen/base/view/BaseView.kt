package com.brain.training.atom.fusion.screen.base.view

interface BaseView {

    fun initProviders()

    fun stopProviders()

    fun updateUi()
}
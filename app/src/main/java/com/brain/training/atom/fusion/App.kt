package com.brain.training.atom.fusion

import android.os.Bundle
import androidx.multidex.MultiDexApplication
import com.brain.training.atom.fusion.di.component.AppComponent
import com.brain.training.atom.fusion.di.component.DaggerAppComponent
import com.brain.training.atom.fusion.di.module.AppModule
import com.brain.training.atom.fusion.provider.AdsProvider
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject


const val WEB_APP_URL = "https://play.google.com/store/apps/details?id=com.brain.training.atom.fusion"

const val ANALYTICS_REMOVE_ADS_CLICK = "remove_ads_click"
const val ANALYTICS_LEVEL_START = "level_start"
const val ANALYTICS_LEVEL_END = "level_end"
const val ANALYTICS_ACCELERATOR_USED = "accelerator_used"
const val ANALYTICS_LEADERBOARD_CLICK = "leaderboard_click"
const val ANALYTICS_RATE_US_CLICK = "rate_us_click"
const val ANALYTICS_TOTAL_ADS_SHOWN = "total_ads_shown"
const val ANALYTICS_AD_SHOWN = "ad_shown"
const val ANALYTICS_START_PURCHASE = "start_purchase"
const val ANALYTICS_SUCCESS_PURCHASE = "success_purchase"
const val ANALYTICS_CONSUMED_PURCHASE = "consumed_purchase"
const val ANALYTICS_REWARDED_VIDEO_SHOWN = "rewarded_video_shown"
const val ANALYTICS_REWARDED_VIDEO_REWARDED = "rewarded_video_rewarded"
const val ANALYTICS_GIFT_NOTIFICATION_SHOWN = "gift_notification_shown"
const val ANALYTICS_REMINDER_NOTIFICATION_SHOWN = "reminder_notification_shown"

const val BUNDLE_KEY_NETWORK = "network"
const val BUNDLE_KEY_SKU = "sku"

class App : MultiDexApplication() {
    val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }
    @Inject
    lateinit var adsProvider: AdsProvider

    companion object {
        lateinit var instance: App

        fun logEvent(event: String, bundle: Bundle) {
            FirebaseAnalytics.getInstance(instance).logEvent(event, bundle)
        }
    }

    override fun onCreate() {
        super.onCreate()
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            return
//        }
//        LeakCanary.install(this)

        instance = this
        component.inject(this)

        initAds()

//        if (!BuildConfig.DEBUG) {
//            Fabric.with(this, Crashlytics())
//        }
    }

    private fun initAds() {
        adsProvider.init()
    }

    override fun onTerminate() {
        adsProvider.onDestroy()
        super.onTerminate()
    }
}
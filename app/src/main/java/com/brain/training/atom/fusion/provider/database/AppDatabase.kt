package com.brain.training.atom.fusion.provider.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.brain.training.atom.fusion.provider.database.dao.UserDao
import com.brain.training.atom.fusion.provider.database.model.User

@Database(entities = arrayOf(User::class), version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
}
package com.brain.training.atom.fusion.provider

import com.brain.training.atom.fusion.BuildConfig
import com.brain.training.atom.fusion.R
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings

private const val ADMOB_APP_ID = "admob_app_id"
private const val ADMOB_VIDEO_60_DOLLAR_ID = "admob_video_60_dollar_id"
private const val ADMOB_VIDEO_55_DOLLAR_ID = "admob_video_55_dollar_id"
private const val ADMOB_VIDEO_50_DOLLAR_ID = "admob_video_50_dollar_id"
private const val ADMOB_VIDEO_45_DOLLAR_ID = "admob_video_45_dollar_id"
private const val ADMOB_VIDEO_40_DOLLAR_ID = "admob_video_40_dollar_id"
private const val ADMOB_VIDEO_35_DOLLAR_ID = "admob_video_35_dollar_id"
private const val ADMOB_VIDEO_30_DOLLAR_ID = "admob_video_30_dollar_id"
private const val ADMOB_VIDEO_25_DOLLAR_ID = "admob_video_25_dollar_id"
private const val ADMOB_VIDEO_20_DOLLAR_ID = "admob_video_20_dollar_id"
private const val ADMOB_VIDEO_18_DOLLAR_ID = "admob_video_18_dollar_id"
private const val ADMOB_VIDEO_16_DOLLAR_ID = "admob_video_16_dollar_id"
private const val ADMOB_VIDEO_14_DOLLAR_ID = "admob_video_14_dollar_id"
private const val ADMOB_VIDEO_12_DOLLAR_ID = "admob_video_12_dollar_id"
private const val ADMOB_VIDEO_10_DOLLAR_ID = "admob_video_10_dollar_id"
private const val ADMOB_VIDEO_9_DOLLAR_ID = "admob_video_9_dollar_id"
private const val ADMOB_VIDEO_8_DOLLAR_ID = "admob_video_8_dollar_id"
private const val ADMOB_VIDEO_7_DOLLAR_ID = "admob_video_7_dollar_id"
private const val ADMOB_VIDEO_6_DOLLAR_ID = "admob_video_6_dollar_id"
private const val ADMOB_VIDEO_5_DOLLAR_ID = "admob_video_5_dollar_id"
private const val ADMOB_VIDEO_4_DOLLAR_ID = "admob_video_4_dollar_id"
private const val ADMOB_VIDEO_3_DOLLAR_ID = "admob_video_3_dollar_id"
private const val ADMOB_VIDEO_2_DOLLAR_ID = "admob_video_2_dollar_id"
private const val ADMOB_VIDEO_1_DOLLAR_ID = "admob_video_1_dollar_id"
private const val ADMOB_VIDEO_REAL_TIME_ID = "admob_video_real_time_id"
private const val ADMOB_INTERSTITIAL_10_DOLLAR_ID = "admob_interstitial_10_dollar_id"
private const val ADMOB_INTERSTITIAL_9_DOLLAR_ID = "admob_interstitial_9_dollar_id"
private const val ADMOB_INTERSTITIAL_8_DOLLAR_ID = "admob_interstitial_8_dollar_id"
private const val ADMOB_INTERSTITIAL_7_DOLLAR_ID = "admob_interstitial_7_dollar_id"
private const val ADMOB_INTERSTITIAL_6_DOLLAR_ID = "admob_interstitial_6_dollar_id"
private const val ADMOB_INTERSTITIAL_5_DOLLAR_ID = "admob_interstitial_5_dollar_id"
private const val ADMOB_INTERSTITIAL_4_DOLLAR_ID = "admob_interstitial_4_dollar_id"
private const val ADMOB_INTERSTITIAL_3_DOLLAR_ID = "admob_interstitial_3_dollar_id"
private const val ADMOB_INTERSTITIAL_2_DOLLAR_ID = "admob_interstitial_2_dollar_id"
private const val ADMOB_INTERSTITIAL_1_DOLLAR_ID = "admob_interstitial_1_dollar_id"
private const val ADMOB_INTERSTITIAL_REAL_TIME = "admob_interstitial_real_time"
private const val ADMOB_BANNER_10_ID = "admob_banner_10_id"
private const val ADMOB_BANNER_9_ID = "admob_banner_9_id"
private const val ADMOB_BANNER_8_ID = "admob_banner_8_id"
private const val ADMOB_BANNER_7_ID = "admob_banner_7_id"
private const val ADMOB_BANNER_6_ID = "admob_banner_6_id"
private const val ADMOB_BANNER_5_ID = "admob_banner_5_id"
private const val ADMOB_BANNER_4_ID = "admob_banner_4_id"
private const val ADMOB_BANNER_3_ID = "admob_banner_3_id"
private const val ADMOB_BANNER_2_ID = "admob_banner_2_id"
private const val ADMOB_BANNER_1_ID = "admob_banner_1_id"
private const val ADMOB_BANNER_05_ID = "admob_banner_05_id"
private const val ADMOB_BANNER_REAL_TIME_ID = "admob_banner_real_time_id"
private const val SHOW_GAME_BANNER = "show_game_banner"
private const val INTERSTITIAL_INTERVAL = "interstitial_interval"
private const val VIDEO_REWARD_COUNT = "video_reward_count"
private const val TAPPX_2_DOLLAR_ID = "tappx_2_dollar_id"
private const val TAPPX_REAL_TIME_ID = "tappx_real_time_id"
private const val ADCOLONY_APP_ID = "adcolony_app_id"
private const val ADCOLONY_VIDEO_ZONE_ID = "adcolony_video_zone_id"

class RemoteConfigImpl : RemoteConfig{
    override val tappxRealTimeId: String
        get() = FirebaseRemoteConfig.getInstance().getString(TAPPX_REAL_TIME_ID)
    override val tappx2dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(TAPPX_2_DOLLAR_ID)
    override val interstitialInterval: Int
        get() = FirebaseRemoteConfig.getInstance().getLong(INTERSTITIAL_INTERVAL).toInt()
    override val banner10DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_BANNER_10_ID)
    override val banner9DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_BANNER_9_ID)
    override val banner8DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_BANNER_8_ID)
    override val banner7DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_BANNER_7_ID)
    override val banner6DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_BANNER_6_ID)
    override val banner5DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_BANNER_5_ID)
    override val banner4DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_BANNER_4_ID)
    override val banner3DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_BANNER_3_ID)
    override val banner2DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_BANNER_2_ID)
    override val banner1DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_BANNER_1_ID)
    override val banner05DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_BANNER_05_ID)
    override val bannerRealTimeId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_BANNER_REAL_TIME_ID)
    override val showGameBanner: Boolean
        get() = FirebaseRemoteConfig.getInstance().getBoolean(SHOW_GAME_BANNER)
    override val adsAppId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_APP_ID)
    override val interstitial10DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_INTERSTITIAL_10_DOLLAR_ID)
    override val interstitial9DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_INTERSTITIAL_9_DOLLAR_ID)
    override val interstitial8DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_INTERSTITIAL_8_DOLLAR_ID)
    override val interstitial7DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_INTERSTITIAL_7_DOLLAR_ID)
    override val interstitial6DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_INTERSTITIAL_6_DOLLAR_ID)
    override val interstitial5DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_INTERSTITIAL_5_DOLLAR_ID)
    override val interstitial4DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_INTERSTITIAL_4_DOLLAR_ID)
    override val interstitial3DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_INTERSTITIAL_3_DOLLAR_ID)
    override val interstitial2DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_INTERSTITIAL_2_DOLLAR_ID)
    override val interstitial1DollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_INTERSTITIAL_1_DOLLAR_ID)
    override val interstitialRealTimeId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_INTERSTITIAL_REAL_TIME)
    override val rewardedVideo60dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_60_DOLLAR_ID)
    override val rewardedVideo55dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_55_DOLLAR_ID)
    override val rewardedVideo50dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_50_DOLLAR_ID)
    override val rewardedVideo45dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_45_DOLLAR_ID)
    override val rewardedVideo40dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_40_DOLLAR_ID)
    override val rewardedVideo35dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_35_DOLLAR_ID)
    override val rewardedVideo30dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_30_DOLLAR_ID)
    override val rewardedVideo25dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_25_DOLLAR_ID)
    override val rewardedVideo20dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_20_DOLLAR_ID)
    override val rewardedVideo18dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_18_DOLLAR_ID)
    override val rewardedVideo16dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_16_DOLLAR_ID)
    override val rewardedVideo14dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_14_DOLLAR_ID)
    override val rewardedVideo12dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_12_DOLLAR_ID)
    override val rewardedVideo10dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_10_DOLLAR_ID)
    override val rewardedVideo9dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_9_DOLLAR_ID)
    override val rewardedVideo8dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_8_DOLLAR_ID)
    override val rewardedVideo7dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_7_DOLLAR_ID)
    override val rewardedVideo6dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_6_DOLLAR_ID)
    override val rewardedVideo5dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_5_DOLLAR_ID)
    override val rewardedVideo4dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_4_DOLLAR_ID)
    override val rewardedVideo3dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_3_DOLLAR_ID)
    override val rewardedVideo2dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_2_DOLLAR_ID)
    override val rewardedVideo1dollarId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_1_DOLLAR_ID)
    override val rewardedVideoRealTimeId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADMOB_VIDEO_REAL_TIME_ID)
    override val adColonyAppId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADCOLONY_APP_ID)
    override val adColonyVideoZoneId: String
        get() = FirebaseRemoteConfig.getInstance().getString(ADCOLONY_VIDEO_ZONE_ID)
    override val videoReward: Int
        get() = FirebaseRemoteConfig.getInstance().getLong(VIDEO_REWARD_COUNT).toInt()

    init {
        val config = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build()
        config.setConfigSettings(configSettings)
        config.setDefaults(R.xml.remote_config_defaults)

        var cacheExpiration: Long = 3600 // 1 hour in seconds.

        if (config.info.configSettings.isDeveloperModeEnabled) {
            cacheExpiration = 0
        }

        config.fetch(cacheExpiration).addOnCompleteListener({ task ->
            if (task.isSuccessful) {
                config.activateFetched()
            }
        })
    }
}

interface RemoteConfig {
    val adsAppId: String

    val rewardedVideo60dollarId: String
    val rewardedVideo55dollarId: String
    val rewardedVideo50dollarId: String
    val rewardedVideo45dollarId: String
    val rewardedVideo40dollarId: String
    val rewardedVideo35dollarId: String
    val rewardedVideo30dollarId: String
    val rewardedVideo25dollarId: String
    val rewardedVideo20dollarId: String
    val rewardedVideo18dollarId: String
    val rewardedVideo16dollarId: String
    val rewardedVideo14dollarId: String
    val rewardedVideo12dollarId: String
    val rewardedVideo10dollarId: String
    val rewardedVideo9dollarId: String
    val rewardedVideo8dollarId: String
    val rewardedVideo7dollarId: String
    val rewardedVideo6dollarId: String
    val rewardedVideo5dollarId: String
    val rewardedVideo4dollarId: String
    val rewardedVideo3dollarId: String
    val rewardedVideo2dollarId: String
    val rewardedVideo1dollarId: String
    val rewardedVideoRealTimeId: String

    val interstitial10DollarId: String
    val interstitial9DollarId: String
    val interstitial8DollarId: String
    val interstitial7DollarId: String
    val interstitial6DollarId: String
    val interstitial5DollarId: String
    val interstitial4DollarId: String
    val interstitial3DollarId: String
    val interstitial2DollarId: String
    val interstitial1DollarId: String
    val interstitialRealTimeId: String

    val banner10DollarId: String
    val banner9DollarId: String
    val banner8DollarId: String
    val banner7DollarId: String
    val banner6DollarId: String
    val banner5DollarId: String
    val banner4DollarId: String
    val banner3DollarId: String
    val banner2DollarId: String
    val banner1DollarId: String
    val banner05DollarId: String
    val bannerRealTimeId: String

    val tappx2dollarId: String
    val tappxRealTimeId: String

    val adColonyAppId: String
    val adColonyVideoZoneId: String

    val showGameBanner: Boolean
    val interstitialInterval: Int
    val videoReward: Int
}
package com.brain.training.atom.fusion.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.brain.training.atom.fusion.R
import com.brain.training.atom.fusion.model.Element
import com.brain.training.atom.fusion.model.StoreItem
import com.brain.training.atom.fusion.ui.AddPulseDrawable
import kotlinx.android.synthetic.main.item_store.view.*
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.sdk27.coroutines.onClick

class StoreAdapter(
        private var items: List<StoreItem>,
        private val listener: StoreClickListener) : RecyclerView.Adapter<StoreAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_store, parent, false)).apply {
            itemView.onClick { listener.onClick(items[adapterPosition]) }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun updateItems(items: List<StoreItem>) {
        this.items = items
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItem(item: StoreItem) {
            itemView.title.text = item.title
            itemView.price.text = item.price
            itemView.image.apply {
                imageResource = item.iconRes
                if (item.iconRes == R.drawable.accelerator_icon) {
                    background = AddPulseDrawable(ContextCompat.getColor(itemView.context, Element.MO.color))
                }
            }
        }
    }

    interface StoreClickListener {

        fun onClick(item: StoreItem)
    }
}
package com.brain.training.atom.fusion.provider

import android.os.Bundle
import android.view.ViewGroup
import com.brain.training.atom.fusion.ANALYTICS_REWARDED_VIDEO_REWARDED
import com.brain.training.atom.fusion.ANALYTICS_REWARDED_VIDEO_SHOWN
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.ads.AdCloseListener
import com.brain.training.atom.fusion.ads.AdmobWrapper
import com.brain.training.atom.fusion.ads.AdsWrapper
import com.brain.training.atom.fusion.ads.TappxWrapper
import com.brain.training.atom.fusion.screen.base.view.BaseActivity
import com.google.android.gms.ads.*
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener

class AdmobProvider(
        private val app: App,
        private val remoteConfig: RemoteConfig,
        private val dataProvider: DataProvider,
        private val playGamesProvider: PlayGamesProvider) : AdsProvider {
    private var videoRequestCounter: Int = 0
    private var videoAdsCounter: Int = 0
    private val interstitialList = mutableListOf<AdsWrapper>()
    private val bannerList = mutableListOf<AdView>()
    private val activitiesList = mutableListOf<BaseActivity>()
    private val videoAdIds = mutableListOf<String>()
    private var rewardedVideoAd: RewardedVideoAd? = null
    private var videoListener: VideoAdListener? = null

    override fun getBanner(): AdView {
        return if (bannerList.isNotEmpty()) { bannerList[0] } else { getBanner(remoteConfig.bannerRealTimeId, "real-time default")}
    }

    override fun init() {
        MobileAds.initialize(app, remoteConfig.adsAppId)

        val adCloseListener = object : AdCloseListener {
            override fun onAdClosed() {
                notifyAdClosed()
            }
        }

        interstitialList.add(AdmobWrapper(remoteConfig.interstitial10DollarId, "Admob 10D", adCloseListener))
        interstitialList.add(AdmobWrapper(remoteConfig.interstitial9DollarId, "Admob 9D", adCloseListener))
        interstitialList.add(AdmobWrapper(remoteConfig.interstitial8DollarId, "Admob 8D", adCloseListener))
        interstitialList.add(AdmobWrapper(remoteConfig.interstitial7DollarId, "Admob 7D", adCloseListener))
        interstitialList.add(AdmobWrapper(remoteConfig.interstitial6DollarId, "Admob 6D", adCloseListener))
        interstitialList.add(AdmobWrapper(remoteConfig.interstitial5DollarId, "Admob 5D", adCloseListener))
        interstitialList.add(AdmobWrapper(remoteConfig.interstitial4DollarId, "Admob 4D", adCloseListener))
        interstitialList.add(AdmobWrapper(remoteConfig.interstitial3DollarId, "Admob 3D", adCloseListener))
        interstitialList.add(AdmobWrapper(remoteConfig.interstitial2DollarId, "Admob 2D", adCloseListener))
        interstitialList.add(TappxWrapper(remoteConfig.tappx2dollarId, "Tappx 2D", adCloseListener))
        interstitialList.add(AdmobWrapper(remoteConfig.interstitial1DollarId, "Admob 1D", adCloseListener))
        interstitialList.add(AdmobWrapper(remoteConfig.interstitialRealTimeId, "Admob RealTime", adCloseListener))
        interstitialList.add(TappxWrapper(remoteConfig.tappxRealTimeId, "Tappx RealTime", adCloseListener))

//        bannerList.add(getBanner(remoteConfig.banner10DollarId, "10D"))
//        bannerList.add(getBanner(remoteConfig.banner9DollarId, "9D"))
//        bannerList.add(getBanner(remoteConfig.banner8DollarId, "8D"))
//        bannerList.add(getBanner(remoteConfig.banner7DollarId, "7D"))
//        bannerList.add(getBanner(remoteConfig.banner6DollarId, "6D"))
        bannerList.add(getBanner(remoteConfig.banner5DollarId, "5D"))
        bannerList.add(getBanner(remoteConfig.banner4DollarId, "4D"))
        bannerList.add(getBanner(remoteConfig.banner3DollarId, "3D"))
        bannerList.add(getBanner(remoteConfig.banner2DollarId, "2D"))
        bannerList.add(getBanner(remoteConfig.banner1DollarId, "1D"))
        bannerList.add(getBanner(remoteConfig.banner05DollarId, "0.5D"))
        bannerList.add(getBanner(remoteConfig.bannerRealTimeId, "real-time"))

        videoAdIds.add(remoteConfig.rewardedVideo60dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo55dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo50dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo45dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo40dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo35dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo30dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo25dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo20dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo18dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo16dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo14dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo12dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo10dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo9dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo8dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo7dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo6dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo5dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo4dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo3dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo2dollarId)
        videoAdIds.add(remoteConfig.rewardedVideo1dollarId)
        videoAdIds.add(remoteConfig.rewardedVideoRealTimeId)

        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(app)
    }

    private fun startLoadVideo() {
        if (videoAdsCounter == videoAdIds.size - 1) {
            videoAdsCounter = 0
            videoRequestCounter++
        }

        if (videoRequestCounter > 2) {
            videoAdsCounter = 0
            videoRequestCounter = 0

            videoListener?.onFailedToLoad()
            return
        }

        rewardedVideoAd?.loadAd(videoAdIds[videoAdsCounter], AdRequest.Builder().build())
    }

    override fun showVideoAd(listener: VideoAdListener?) {
        videoListener = listener

        if (rewardedVideoAd != null && rewardedVideoAd?.isLoaded!!) {
//            videoListener?.onLoaded()
            rewardedVideoAd?.show()
//            videoListener = null
        } else {
            if (videoAdsCounter == 0 && videoRequestCounter == 0) {
                startLoadVideo()
            }
        }
    }

    override fun isVideoAdLoaded(): Boolean {
        rewardedVideoAd?.let {
            return it.isLoaded
        }
        return false
    }

    private fun getBanner(unitId: String, scenario: String): AdView {
        return AdView(app).apply {
            adSize = AdSize.SMART_BANNER
            adUnitId = unitId
            adListener = object : AdListener() {
                override fun onAdLoaded() {

                }

                override fun onAdFailedToLoad(errorCode: Int) {
                    bannerList.remove(this@apply)
                    this@apply.destroy()
                }
            }
            loadAd(AdRequest.Builder().build())
        }
    }

    override fun subscribe(activity: BaseActivity) {
        rewardedVideoAd?.rewardedVideoAdListener = object : RewardedVideoAdListener {
            override fun onRewardedVideoCompleted() {

            }

            override fun onRewardedVideoAdClosed() {
                startLoadVideo()
                videoListener = null
            }

            override fun onRewardedVideoAdLeftApplication() {}

            override fun onRewardedVideoAdLoaded() {
                videoAdsCounter = 0
                videoListener?.onLoaded()
            }

            override fun onRewardedVideoAdOpened() {
                App.logEvent(ANALYTICS_REWARDED_VIDEO_SHOWN, Bundle())
            }

            override fun onRewarded(reward: RewardItem?) {
                App.logEvent(ANALYTICS_REWARDED_VIDEO_REWARDED, Bundle())
                videoListener?.onRewarded()
            }

            override fun onRewardedVideoStarted() {}

            override fun onRewardedVideoAdFailedToLoad(errorCode: Int) {
                videoAdsCounter++
                startLoadVideo()
            }
        }

        activitiesList.add(activity)
    }

    override fun unsubscribe(activity: BaseActivity) {
        activitiesList.remove(activity)
    }

    private fun notifyAdClosed() {
        for (activity in activitiesList) {
            activity.onAdClosed()
        }
    }

    override fun canShowAds(): Boolean {
        if (dataProvider.getUser().isPro) return false
        if (dataProvider.getUser().adsRemoved) return false

        return true
    }

    override fun showInterstitial() {
        var isAdShowed = false
        for (interstitial in interstitialList) {
            if (interstitial.isLoaded()) {
                interstitial.show()
                isAdShowed = true

                break
            }
        }
        if (!isAdShowed) {
            notifyAdClosed()
        }
    }

    override fun onDestroy() {
        for(ad in interstitialList) {
            interstitialList.remove(ad)
            ad.onDestroy()
        }
        for (banner in bannerList) {
            if (banner.parent != null) {
                (banner.parent as ViewGroup).removeView(banner)
            }
            bannerList.remove(banner)
            banner.destroy()
        }
        rewardedVideoAd?.destroy(app)
    }
}

interface AdsProvider {

    fun getBanner(): AdView

    fun init()

    fun subscribe(activity: BaseActivity)

    fun unsubscribe(activity: BaseActivity)

    fun canShowAds(): Boolean

    fun showInterstitial()

    fun onDestroy()

    fun showVideoAd(listener: VideoAdListener?)

    fun isVideoAdLoaded(): Boolean
}

interface VideoAdListener {

    fun onLoaded()

    fun onRewarded()

    fun onFailedToLoad()
}
package com.brain.training.atom.fusion.di.module

import com.brain.training.atom.fusion.provider.*
import com.brain.training.atom.fusion.screen.game.presenter.GamePresenter
import com.brain.training.atom.fusion.screen.game.view.GameView
import dagger.Module
import dagger.Provides

@Module
class GameModule {

    @Provides
    fun providePresenter(
            dataProvider: DataProvider,
            adsProvider: AdsProvider,
            remoteConfigProvider: RemoteConfig,
            playGamesProvider: PlayGamesProvider,
            soundProvider: SoundProvider): GamePresenter<GameView> = GamePresenter(dataProvider, adsProvider, remoteConfigProvider, playGamesProvider, soundProvider)
}
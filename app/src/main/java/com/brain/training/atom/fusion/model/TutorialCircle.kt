package com.brain.training.atom.fusion.model

data class TutorialCircle(val x: Float, val y: Float, val radius: Float)
package com.brain.training.atom.fusion.provider.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "User")
data class User(
        @ColumnInfo(name = "id")
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        @ColumnInfo(name = "name") var name: String = "",
        @ColumnInfo(name = "isPro") var isPro: Boolean = false,
        @ColumnInfo(name = "adsRemoved") var adsRemoved: Boolean = false,
        @ColumnInfo(name = "standardModeScore") var standardModeScore: Int = 0,
        @ColumnInfo(name = "standardModeProgress") var standardModeProgress: Int = 1,
        @ColumnInfo(name = "accelerators") var accelerators: Int = 1,
        @ColumnInfo(name = "protons") var protons: Int = 30) {
        @Ignore constructor() : this(0)
}
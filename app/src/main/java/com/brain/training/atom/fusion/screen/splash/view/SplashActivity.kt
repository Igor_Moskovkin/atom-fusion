package com.brain.training.atom.fusion.screen.splash.view

import android.os.Bundle
import android.preference.PreferenceManager
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.R
import com.brain.training.atom.fusion.di.module.SplashModule
import com.brain.training.atom.fusion.provider.DataProvider
import com.brain.training.atom.fusion.provider.GiftProvider.Companion.REMINDER_GIFT_ACCELERATORS_COUNT
import com.brain.training.atom.fusion.provider.GiftProvider.Companion.REMINDER_GIFT_PROTONS_COUNT
import com.brain.training.atom.fusion.provider.NotificationProvider
import com.brain.training.atom.fusion.provider.NotificationProvider.Companion.REMINDER_NOTIFICATION_SHOWN
import com.brain.training.atom.fusion.provider.PlayGamesProvider
import com.brain.training.atom.fusion.screen.base.view.BaseActivity
import com.brain.training.atom.fusion.screen.menu.view.MenuActivity
import com.brain.training.atom.fusion.screen.splash.presenter.SplashPresenter
import org.jetbrains.anko.intentFor
import javax.inject.Inject


class SplashActivity : BaseActivity(), SplashView {
    override val layoutResId: Int = R.layout.activity_splash
    @Inject
    lateinit var presenter: SplashPresenter<SplashView>
    @Inject
    lateinit var notificationProvider: NotificationProvider
    @Inject
    lateinit var dataProvider: DataProvider
    @Inject
    lateinit var playGamesProvider: PlayGamesProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun performDi() {
        App.instance.component.plus(SplashModule()).inject(this)
    }

    override fun openMainMenu() {
        startActivity(intentFor<MenuActivity>())
        finish()
    }

    override fun updateUi() {
    }

    override fun checkForGift() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)

        if (prefs.getInt(REMINDER_NOTIFICATION_SHOWN, 0) >= 1) {
            dataProvider.changeProtonsCount(REMINDER_GIFT_PROTONS_COUNT)
            dataProvider.changeAcceleratorsCount(REMINDER_GIFT_ACCELERATORS_COUNT)
            playGamesProvider.submitAddProtons(this, REMINDER_GIFT_PROTONS_COUNT)
        }

        prefs.edit().putInt(REMINDER_NOTIFICATION_SHOWN, 0).apply()

        notificationProvider.scheduleRemindNotification()
    }
}
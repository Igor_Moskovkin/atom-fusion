package com.brain.training.atom.fusion.di.component

import com.brain.training.atom.fusion.di.module.MenuModule
import com.brain.training.atom.fusion.screen.menu.view.MenuActivity
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(MenuModule::class))
interface MenuComponent {

    fun inject(menuActivity: MenuActivity)
}
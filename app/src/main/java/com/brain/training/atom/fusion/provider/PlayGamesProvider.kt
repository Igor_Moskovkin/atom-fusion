package com.brain.training.atom.fusion.provider

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.games.Games


private const val RC_SIGN_IN: Int = 3854
private const val RC_LEADERBOARD_UI = 5983

class PlayGamesProvider(val app: App, private val authProvider: AuthProvider) {
    private val googleSignInClient: GoogleSignInClient by lazy {
        GoogleSignIn.getClient(
                app,
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                        .requestServerAuthCode(app.getString(R.string.default_web_client_id))
                        .build())
    }

//    fun onCreate(activity: Activity) {}

    fun onStart(activity: Activity) {
        if (!isSignedIn()) {
            signInSilently(activity)
        } else {
            val lastSignedInAccount = GoogleSignIn.getLastSignedInAccount(activity)

            if (lastSignedInAccount != null) {
                Games.getGamesClient(activity, lastSignedInAccount).setViewForPopups(activity.window.decorView)
            } else {
                startSignInIntent(activity)
            }
        }
    }

//    fun onResume(activity: Activity) {}
//
//    fun onPause(activity: Activity) {}
//
//    fun onStop(activity: Activity) {}
//
//    fun onDestroy() {}

    fun onActivityResult(activity: Activity, requestCode: Int, resultCode: Int, intent: Intent?) {
        if (requestCode == RC_SIGN_IN && resultCode == AppCompatActivity.RESULT_OK) {
            try {
                onConnected(
                        activity,
                        GoogleSignIn.getSignedInAccountFromIntent(intent).getResult(ApiException::class.java))
            } catch (apiException: ApiException) {
                onDisconnected()
            }
        } else {
            onDisconnected()
        }
    }

    private fun onConnected(activity: Activity, account: GoogleSignInAccount?) {
        account?.let {
            Games.getGamesClient(activity, account).setViewForPopups(activity.window.decorView)

            authProvider.signIn(activity, account)
        }
    }

    private fun onDisconnected() {
    }

    private fun isSignedIn(): Boolean {
        return GoogleSignIn.getLastSignedInAccount(app) != null
    }

    private fun startSignInIntent(activity: Activity) {
        if (checkPlayServices()) {
            val intent = googleSignInClient.signInIntent
            activity.startActivityForResult(intent, RC_SIGN_IN)
        }
    }

    private fun signInSilently(activity: Activity) {
        if (checkPlayServices()) {
            googleSignInClient.silentSignIn().addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
                    onConnected(activity, task.result)
                } else {
                    startSignInIntent(activity)
                }
            }
        }
    }

    fun submitScore(activity: Activity, score: Long, leaderboard: String) {
        val lastSignedInAccount = GoogleSignIn.getLastSignedInAccount(activity)

        lastSignedInAccount?.let {
            Games.getLeaderboardsClient(activity, it).submitScore(leaderboard, score)
        }
    }

    fun showLeaderboard(activity: Activity, leaderboard: String) {
        val lastSignedInAccount = GoogleSignIn.getLastSignedInAccount(activity)

        lastSignedInAccount?.let {
            Games.getLeaderboardsClient(activity, it).getLeaderboardIntent(leaderboard)?.addOnSuccessListener { intent ->
                activity.startActivityForResult(intent, RC_LEADERBOARD_UI)
            }
        }
    }

    private fun checkPlayServices(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(App.instance)
        if (resultCode != ConnectionResult.SUCCESS) {
            return false
        }
        return true
    }

    fun submitAddProtons(activity: Activity?, count: Int) {
        activity?.let {
            val lastSignedInAccount = GoogleSignIn.getLastSignedInAccount(activity)

            lastSignedInAccount?.let {
                Games.getEventsClient(activity, lastSignedInAccount).increment(app.getString(R.string.event_protons_add), count)
            }
        }
    }

    fun submitSpendProtons(activity: Activity?, count: Int) {
        activity?.let {
            val lastSignedInAccount = GoogleSignIn.getLastSignedInAccount(activity)

            lastSignedInAccount?.let {
                Games.getEventsClient(activity, lastSignedInAccount).increment(app.getString(R.string.event_protons_spend), count)
            }
        }
    }

//    private fun signOut() {
//        googleSignInClient.signOut()
//    }
}
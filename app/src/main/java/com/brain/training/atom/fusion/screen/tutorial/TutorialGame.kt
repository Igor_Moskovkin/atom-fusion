package com.brain.training.atom.fusion.screen.tutorial

import android.animation.ValueAnimator
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.transition.AutoTransition
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.R
import com.brain.training.atom.fusion.model.Atom
import com.brain.training.atom.fusion.model.Element
import com.brain.training.atom.fusion.model.TutorialCircle
import com.brain.training.atom.fusion.screen.game.view.ATOM_SPAWN_DURATION
import com.brain.training.atom.fusion.screen.game.view.GameActivity
import com.brain.training.atom.fusion.screen.menu.presenter.TUTORIAL_SHOWN
import com.brain.training.atom.fusion.ui.AddPulseDrawable
import com.brain.training.atom.fusion.ui.AtomView
import com.brain.training.atom.fusion.ui.TutorialView
import com.github.florent37.viewanimator.ViewAnimator
import kotlinx.android.synthetic.main.activity_game.*
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk27.coroutines.onClick

private const val TUTORIAL_COUNTER = "tutorial_counter"
private const val TUTORIAL_VIEW_TAG = "tutorial_view_tag"

class TutorialGame : GameActivity() {
    private val startAtoms: List<List<Element>> by lazy {
        listOf(
                listOf(Element.H, Element.B, Element.H, Element.BE, Element.HE, Element.P),
                listOf(Element.B, Element.H, Element.H, Element.BE, Element.BE, Element.P),
                listOf(Element.P, Element.P, Element.BE, Element.BE, Element.BE, Element.BE),
                listOf(Element.P, Element.P, Element.B, Element.BE, Element.H, Element.H, Element.BE, Element.B),
                listOf(Element.HE, Element.H, Element.B, Element.BE, Element.F, Element.H, Element.BE, Element.B),
                listOf(Element.F, Element.H, Element.B, Element.BE, Element.F, Element.H, Element.B, Element.B),
                listOf(Element.H, Element.B, Element.BE, Element.F, Element.P, Element.O, Element.O, Element.BE, Element.H, Element.P, Element.O)
        )
    }
    private val nextAtoms: List<Element> by lazy {
        listOf(
                Element.BE,
                Element.PLUS,
                Element.PLUS,
                Element.PLUS,
                Element.MINUS,
                Element.MINUS,
                Element.H
        )
    }
    private val tutorialTexts: Array<String> by lazy {
        resources.getStringArray(R.array.tutorial_texts)
    }
    private val tutorialCounter: Int by lazy {
        intent.getIntExtra(TUTORIAL_COUNTER, 0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        storeBtn.onClick {
            if (canTouch) {
                root.removeView(root.findViewWithTag(TUTORIAL_VIEW_TAG))

                spawnAccelerator()

                val atomOne = atoms[4]
                val atomTwo = atoms[5]

                val centerX = ((atomOne.x - atomTwo.x) / 2) + atomOne.width / 2 + atomTwo.x
                val centerY = ((atomOne.y - atomTwo.y) / 2) + atomOne.height * 0.8F + atomTwo.y

                val circles = mutableListOf(
                        TutorialCircle(centerX, centerY, atomSide.toFloat()),
                        TutorialCircle(atomSpawner.x + atomSpawner.width / 2, atomSpawner.y + atomSpawner.height / 2, atomSide.toFloat())
                )

                val tutorial = TutorialView(this@TutorialGame, circles, tutorialTexts[tutorialCounter]).apply {
                    tag = TUTORIAL_VIEW_TAG
                }

                root.addView(tutorial)
            }
        }
    }

    override fun spawnStartAtoms() {
        val params = ConstraintLayout.LayoutParams(ConstraintSet.WRAP_CONTENT, ConstraintSet.WRAP_CONTENT).apply {
            bottomToBottom = atomSpawner.id
            topToTop = atomSpawner.id
            startToStart = atomSpawner.id
            endToEnd = atomSpawner.id
        }

        (window.decorView.background as LayerDrawable).apply {
            (findDrawableByLayerId(R.id.circle) as GradientDrawable).apply {
                colors = intArrayOf(ContextCompat.getColor(this@TutorialGame, Element.H.color), Color.TRANSPARENT)
                alpha = 45
            }
        }

        for (i in startAtoms[tutorialCounter].indices) {
            var angle = i * (360 / startAtoms[tutorialCounter].size) + 10
            if (angle == 360) {
                angle = 0
            }

            val view = AtomView(this, Atom(startAtoms[tutorialCounter][i]), atomSide).apply {
                scaleX = 0F
                scaleY = 0F
                layoutParams = params
                id = View.generateViewId()
            }
            root.addView(view)

            ViewAnimator.animate(view)
                    .duration(ATOM_SPAWN_DURATION)
                    .scale(0F, 1F)
                    .thenAnimate(view)
                    .duration(ATOM_SPAWN_DURATION / 10)
                    .pulse()
                    .onStop {
                        val circleParams = ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
                            circleRadius = 0
                            circleAngle = angle.toFloat()
                            circleConstraint = atomSpawner.id
                        }
                        view.layoutParams = circleParams

                        val animator = ValueAnimator.ofInt(0, atomRadius).apply {
                            duration = ATOM_SPAWN_DURATION / 2
                            interpolator = AccelerateDecelerateInterpolator()
                            addUpdateListener {
                                circleParams.circleRadius = it.animatedValue as Int
                                view.layoutParams = circleParams
                            }
                        }
                        animator.start()

                        atoms.add(view)

                        if (i == 5) {
                            presenter.onHighestAtomCheck(getCurrentHighestAtom())
                        }
                    }
                    .start()
        }
    }

    override fun spawnAtom() {
        if (root.findViewWithTag<View?>(TUTORIAL_VIEW_TAG) == null) {
            super.spawnAtom()

            atomSpawner.postDelayed({
                var centerX = 0F
                var centerY = 0F

                when (tutorialCounter) {
                    0, 1, 2 -> {
                        val atomOne = atoms[3]
                        val atomTwo = atoms[4]

                        centerX = ((atomOne.x - atomTwo.x) / 2) + atomOne.width / 4 + atomTwo.x
                        centerY = ((atomOne.y - atomTwo.y) / 2) + atomOne.height * 0.8F + atomTwo.y
                    }
                    3 -> {
                        val atomOne = atoms[4]
                        val atomTwo = atoms[5]

                        centerX = ((atomOne.x - atomTwo.x) / 2) + atomOne.width / 4 + atomTwo.x
                        centerY = ((atomOne.y - atomTwo.y) / 2) + atomOne.height * 0.8F + atomTwo.y
                    }
                    4, 5 -> {
                        val atomOne = atoms[5]

                        centerX = ((atomOne.x + atomOne.width / 2))
                        centerY = ((atomOne.y + atomOne.height / 2))
                    }
                    6 -> {
                        centerX = storeBtn.x + storeBtn.width / 2
                        centerY = storeBtn.y + storeBtn.height / 2
                    }
                }

                val circles = mutableListOf(
                        TutorialCircle(centerX, centerY, atomSide.toFloat()),
                        TutorialCircle(atomSpawner.x + atomSpawner.width / 2, atomSpawner.y + atomSpawner.height / 2, atomSide.toFloat())
                )

                val tutorial = TutorialView(this@TutorialGame, circles, tutorialTexts[tutorialCounter]).apply {
                    tag = TUTORIAL_VIEW_TAG
                }

                root.addView(tutorial)
            }, 500)
        } else {
            root.removeView(root.findViewWithTag(TUTORIAL_VIEW_TAG))

            val doneView = ImageView(this@TutorialGame).apply {
                layoutParams = ConstraintLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
                imageResource = R.drawable.ic_check_circle
                scaleType = ImageView.ScaleType.CENTER_INSIDE
                onClick {
                    if (tutorialCounter == tutorialTexts.size - 1) {
                        val prefs = PreferenceManager.getDefaultSharedPreferences(App.instance)
                        prefs.edit().putBoolean(TUTORIAL_SHOWN, true).apply()

                        startActivity(intentFor<GameActivity>())
                    } else {
                        startActivity(intentFor<TutorialGame>(TUTORIAL_COUNTER to tutorialCounter + 1))
                    }
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                    finish()
                }
            }

            root.addView(doneView)

            ViewAnimator.animate(doneView)
                    .duration(500)
                    .scale(0F, 1F)
                    .start()
        }
    }

    override fun getNextAtom(): Element {
        return nextAtoms[tutorialCounter]
    }

    override fun returnAtom(atom: AtomView) {
        returnedAtomIndex = atoms.indexOf(atom)

        val params = ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
            bottomToBottom = atomSpawner.id
            topToTop = atomSpawner.id
            startToStart = atomSpawner.id
            endToEnd = atomSpawner.id
        }

        if (spawnedAtom.atom.element.atom < 0) {
            root.removeView(root.findViewWithTag(spawnedAtom))
        }
        root.removeView(spawnedAtom)
        spawnedAtom = atom
        atoms.remove(atom)
        directionView.mainColor = ContextCompat.getColor(this, spawnedAtom.atom.element.color)

        TransitionManager.beginDelayedTransition(root, AutoTransition().apply {
            interpolator = AccelerateDecelerateInterpolator()
            duration = 100
            addListener(object : Transition.TransitionListener {
                override fun onTransitionEnd(transition: Transition) {
                    presenter.onRemoveEnd()
                }

                override fun onTransitionResume(transition: Transition) {
                }

                override fun onTransitionPause(transition: Transition) {
                }

                override fun onTransitionCancel(transition: Transition) {
                }

                override fun onTransitionStart(transition: Transition) {
                }

            })
        })
        spawnedAtom.apply {
            layoutParams = params
            setOnClickListener {
                ViewAnimator.animate(spawnedAtom)
                        .duration(150)
                        .scale(1F, 0F)
                        .onStop {
                            root.removeView(root.findViewWithTag(TUTORIAL_VIEW_TAG))

                            spawnedAtom.atom = Atom(Element.PLUS)
                            val pulseView = View(this@TutorialGame).apply {
                                backgroundDrawable = AddPulseDrawable(ContextCompat.getColor(this@TutorialGame, spawnedAtom.atom.element.color))

                                layoutParams = ConstraintLayout.LayoutParams((atomSide * 1.4F).toInt(), (atomSide * 1.4F).toInt()).apply {
                                    leftToLeft = spawnedAtom.id
                                    topToTop = spawnedAtom.id
                                    bottomToBottom = spawnedAtom.id
                                    rightToRight = spawnedAtom.id
                                }

                                tag = spawnedAtom
                            }

                            root.addView(pulseView, root.childCount - 2)

                            val atomOne = atoms[5]
                            val atomTwo = atoms[6]

                            val centerX = ((atomOne.x - atomTwo.x) / 2) + atomOne.width / 4 + atomTwo.x
                            val centerY = ((atomOne.y - atomTwo.y) / 2) + atomOne.height * 0.5F + atomTwo.y

                            val circles = mutableListOf(
                                    TutorialCircle(centerX, centerY, atomSide.toFloat()),
                                    TutorialCircle(atomSpawner.x + atomSpawner.width / 2, atomSpawner.y + atomSpawner.height / 2, atomSide.toFloat())
                            )

                            val tutorial = TutorialView(this@TutorialGame, circles, tutorialTexts[tutorialCounter]).apply {
                                tag = TUTORIAL_VIEW_TAG
                            }

                            root.addView(tutorial)
                        }
                        .thenAnimate(spawnedAtom)
                        .duration(150)
                        .scale(0F, 1F)
                        .thenAnimate(spawnedAtom)
                        .duration(100)
                        .pulse()
                        .start()
                it?.setOnClickListener(null)
                it?.isClickable = false
            }
        }

        root.removeView(root.findViewWithTag(TUTORIAL_VIEW_TAG))

        root.postDelayed({
            var centerX = 0F
            var centerY = 0F

            when (tutorialCounter) {
                4 -> {
                    val atomOne = atoms[1]
                    val atomTwo = atoms[2]

                    centerX = ((atomOne.x - atomTwo.x) / 2) + atomOne.width / 2 + atomTwo.x
                    centerY = ((atomOne.y - atomTwo.y) / 2) + atomOne.height * 0.5F + atomTwo.y
                }
                5 -> {
                    centerX = atomSpawner.x + atomSpawner.width / 2
                    centerY = atomSpawner.y + atomSpawner.height / 2
                }
            }

            val circles = mutableListOf(
                    TutorialCircle(centerX, centerY, atomSide.toFloat()),
                    TutorialCircle(atomSpawner.x + atomSpawner.width / 2, atomSpawner.y + atomSpawner.height / 2, atomSide.toFloat())
            )

            val tutorial = TutorialView(this@TutorialGame, circles, tutorialTexts[tutorialCounter]).apply {
                tag = TUTORIAL_VIEW_TAG
            }

            root.addView(tutorial)
        }, 300)
    }
}
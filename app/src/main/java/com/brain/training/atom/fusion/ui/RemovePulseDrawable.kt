package com.brain.training.atom.fusion.ui

import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.view.animation.DecelerateInterpolator

class RemovePulseDrawable(color: Int) : PulseDrawable(color) {
    override val pulseStartColorOpacity: Int = 150
    override val minimumRadius = 0F
    override val animationDurationInMs = 3000
    override var currentExpandAnimationValue = 255F
    override var currentAlphaAnimationValue = 150

    init {
        initializeDrawable()
    }

    override fun prepareAnimation() {
        val expandAnimator = ValueAnimator.ofFloat(1F, 0.6F)
        expandAnimator.repeatCount = ValueAnimator.INFINITE
        expandAnimator.repeatMode = ValueAnimator.RESTART
        expandAnimator.addUpdateListener { animation ->
            currentExpandAnimationValue = animation.animatedValue as Float
            if (currentExpandAnimationValue == 0F) {
                currentAlphaAnimationValue = 150
            }
            invalidateSelf()
        }
        val alphaAnimator = ValueAnimator.ofInt(0, 150)
        alphaAnimator.repeatCount = ValueAnimator.INFINITE
        alphaAnimator.repeatMode = ValueAnimator.RESTART
        alphaAnimator.addUpdateListener { animation -> currentAlphaAnimationValue = animation.animatedValue as Int }
        val animation = AnimatorSet()
        animation.playTogether(expandAnimator, alphaAnimator)
        animation.duration = animationDurationInMs.toLong()
        animation.interpolator = DecelerateInterpolator()
        animation.start()
    }
}
package com.brain.training.atom.fusion.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.brain.training.atom.fusion.R
import com.brain.training.atom.fusion.adapter.StoreAdapter
import com.brain.training.atom.fusion.model.StoreItem
import com.brain.training.atom.fusion.provider.BillingProvider
import com.brain.training.atom.fusion.provider.DataProvider
import com.brain.training.atom.fusion.screen.base.view.BaseActivity
import kotlinx.android.synthetic.main.dialog_store.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.solovyev.android.checkout.ProductTypes

class DialogStore(
        val baseActivity: Context,
        val billingProvider: BillingProvider,
        val dataProvider: DataProvider,
        private val itemsType: StoreItemType) : AppCompatDialog(baseActivity, R.style.AppTheme_FullScreen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_store)

        window?.attributes?.dimAmount = 0.6f

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)

        storeRecyclerView.apply {
            layoutManager = LinearLayoutManager(baseActivity, LinearLayoutManager.VERTICAL, false)
            adapter = StoreAdapter(getStoreItems(itemsType), object : StoreAdapter.StoreClickListener{
                override fun onClick(item: StoreItem) {
                    billingProvider.launchPurchaseFlow(baseActivity as BaseActivity, item.productType,item.sku)
                }
            })
        }

        close.onClick{
            dismiss()
        }
    }

    private fun getStoreItems(itemType: StoreItemType): List<StoreItem> {
        val items: MutableList<StoreItem> = mutableListOf()

        if (itemType == StoreItemType.ACCELERATOR) {
            val first = billingProvider.getSkuProduct(billingProvider.oneDollarSku)
            first?.let {
                items.add(StoreItem(
                        billingProvider.oneDollarSku,
                        if (it.isInApp) ProductTypes.IN_APP else ProductTypes.SUBSCRIPTION,
                        it.title.split(" (")[0],
                        it.price,
                        R.drawable.accelerator_icon))
            }

            val second = billingProvider.getSkuProduct(billingProvider.twoDollarsSku)
            second?.let {
                items.add(StoreItem(
                        billingProvider.twoDollarsSku,
                        if (it.isInApp) ProductTypes.IN_APP else ProductTypes.SUBSCRIPTION,
                        it.title.split(" (")[0],
                        it.price,
                        R.drawable.accelerator_icon))
            }

            val third = billingProvider.getSkuProduct(billingProvider.threeDollarsSku)
            third?.let {
                items.add(StoreItem(
                        billingProvider.threeDollarsSku,
                        if (it.isInApp) ProductTypes.IN_APP else ProductTypes.SUBSCRIPTION,
                        it.title.split(" (")[0],
                        it.price,
                        R.drawable.accelerator_icon))
            }

            if (dataProvider.getUser().adsRemoved || dataProvider.getUser().isPro) {
                return items
            }

            val removeAds = billingProvider.getSkuProduct(billingProvider.removeAdsSku)
            removeAds?.let {
                items.add(StoreItem(
                        billingProvider.removeAdsSku,
                        if (it.isInApp) ProductTypes.IN_APP else ProductTypes.SUBSCRIPTION,
                        it.title.split(" (")[0],
                        it.price,
                        R.drawable.remove_ads_icon))
            }

            val proVersion = billingProvider.getSkuProduct(billingProvider.proVersionSku)
            proVersion?.let {
                items.add(StoreItem(
                        billingProvider.proVersionSku,
                        if (it.isInApp) ProductTypes.IN_APP else ProductTypes.SUBSCRIPTION,
                        it.title.split(" (")[0],
                        it.price,
                        R.drawable.pro_version_icon))
            }
        } else {
            val first = billingProvider.getSkuProduct(billingProvider.oneDollarParticleSku)
            first?.let {
                items.add(StoreItem(
                        billingProvider.oneDollarParticleSku,
                        if (it.isInApp) ProductTypes.IN_APP else ProductTypes.SUBSCRIPTION,
                        it.title.split(" (")[0],
                        it.price,
                        R.drawable.proton))
            }

            val second = billingProvider.getSkuProduct(billingProvider.twoDollarsParticleSku)
            second?.let {
                items.add(StoreItem(
                        billingProvider.twoDollarsParticleSku,
                        if (it.isInApp) ProductTypes.IN_APP else ProductTypes.SUBSCRIPTION,
                        it.title.split(" (")[0],
                        it.price,
                        R.drawable.proton))
            }

            val third = billingProvider.getSkuProduct(billingProvider.threeDollarsParticleSku)
            third?.let {
                items.add(StoreItem(
                        billingProvider.threeDollarsParticleSku,
                        if (it.isInApp) ProductTypes.IN_APP else ProductTypes.SUBSCRIPTION,
                        it.title.split(" (")[0],
                        it.price,
                        R.drawable.proton))
            }

            val fourth = billingProvider.getSkuProduct(billingProvider.fiveDollarsParticleSku)
            fourth?.let {
                items.add(StoreItem(
                        billingProvider.fiveDollarsParticleSku,
                        if (it.isInApp) ProductTypes.IN_APP else ProductTypes.SUBSCRIPTION,
                        it.title.split(" (")[0],
                        it.price,
                        R.drawable.proton))
            }
        }

        return items
    }

    fun updateUi() {
        (storeRecyclerView.adapter as StoreAdapter).updateItems(getStoreItems(itemsType))
    }

    enum class StoreItemType {
        ACCELERATOR,
        PROTON
    }
}
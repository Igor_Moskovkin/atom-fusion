package com.brain.training.atom.fusion.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatDialog
import com.brain.training.atom.fusion.R
import kotlinx.android.synthetic.main.dialog_question.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.textResource

class DialogQuestion(
        context: Context,
        @StringRes private val message: Int,
        private val listener: View.OnClickListener) : AppCompatDialog(context, R.style.AppTheme_FullScreen) {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_question)

        window?.attributes?.dimAmount = 0.6f

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)

        messageView.textResource = message

        close.onClick {
            dismiss()
        }
        yesBtn.onClick {
            listener.onClick(null)
            dismiss()
        }
        noBtn.onClick {
            dismiss()
        }
    }
}
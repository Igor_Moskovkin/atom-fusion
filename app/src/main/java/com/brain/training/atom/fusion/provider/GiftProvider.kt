package com.brain.training.atom.fusion.provider

import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.text.format.DateUtils
import com.brain.training.atom.fusion.App
import com.brain.training.atom.fusion.model.Gift
import java.util.*


class GiftProvider(
        private val app: App,
        private val notificationProvider: NotificationProvider) {
    private val mPrefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(app)
    }

    fun getNextGiftTime(): Long = mPrefs.getLong(GIFT_TIME, 0)

    fun getNextGift(): Gift {
        val random = Random()
        val giftType = random.nextInt(GIFT_TYPE_COUNT)

        return when (giftType) {
            GIFT_TYPE_1 -> {
                Gift(giftType, random.nextInt(10 - 5 + 1) + 5, 0)
            }
            GIFT_TYPE_2 -> {
                Gift(giftType, random.nextInt(15 - 7 + 1) + 7, 0)
            }
            GIFT_TYPE_3 -> {
                Gift(giftType, random.nextInt(20 - 10 + 1) + 10, random.nextInt(2))
            }
            else -> Gift(0, 0, 0)
        }
    }

    fun scheduleNextGift() {
        val nextGiftMillis = System.currentTimeMillis() + GIFT_INTERVAL

        mPrefs.edit().putLong(GIFT_TIME, nextGiftMillis).apply()
        notificationProvider.scheduleGiftNotification(nextGiftMillis)
    }

    companion object {
        private const val GIFT_TIME = "gift_time"
        private const val GIFT_INTERVAL = DateUtils.HOUR_IN_MILLIS * 4
        private const val GIFT_TYPE_COUNT = 3
        const val GIFT_TYPE_1 = 0
        const val GIFT_TYPE_2 = 1
        const val GIFT_TYPE_3 = 2
        const val REMINDER_GIFT_ACCELERATORS_COUNT = 1
        const val REMINDER_GIFT_PROTONS_COUNT = 20
    }
}
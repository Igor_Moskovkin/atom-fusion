package com.brain.training.atom.fusion.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.GridLayoutManager
import com.brain.training.atom.fusion.R
import com.brain.training.atom.fusion.adapter.AtomStoreAdapter
import com.brain.training.atom.fusion.adapter.AtomStoreListener
import com.brain.training.atom.fusion.model.Element
import com.brain.training.atom.fusion.provider.*
import com.brain.training.atom.fusion.screen.base.view.BaseActivity
import kotlinx.android.synthetic.main.dialog_atom_store.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class DialogAtomStore(
        private val baseActivity: BaseActivity,
        private val dataProvider: DataProvider,
        private val remoteConfig: RemoteConfig,
        private val adsProvider: AdsProvider,
        private val playGamesProvider: PlayGamesProvider,
        private val billingProvider: BillingProvider,
        private val atomSide: Int,
        private val highestAtom: Element,
        private val listener: AtomStoreListener) : AppCompatDialog(baseActivity, R.style.AppTheme_FullScreen) {
    private var loadingDialog: DialogLoading? = null
    private var storeDialog: DialogStore? = null
    private var noAdsDialog: DialogNoAds? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_atom_store)

        window?.attributes?.dimAmount = 0.6f

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(baseActivity, 3)
            adapter = AtomStoreAdapter(dataProvider, atomSide, highestAtom, listener)
        }

        watchAdsBtn.text = context.getString(R.string.watch_video_button_title).format(remoteConfig.videoReward)

        protonsCountText.onClick {
            storeDialog = DialogStore(baseActivity, billingProvider, dataProvider, DialogStore.StoreItemType.PROTON)
            storeDialog?.show()
        }

        playVideoContainer.onClick {
            loadingDialog = DialogLoading(getContext())
            loadingDialog?.setOnDismissListener { loadingDialog = null }
            loadingDialog?.show()

            adsProvider.showVideoAd(object : VideoAdListener {
                override fun onRewarded() {
                    playGamesProvider.submitAddProtons(baseActivity, remoteConfig.videoReward)
                    dataProvider.changeProtonsCount(remoteConfig.videoReward)

                    loadingDialog?.dismiss()

                    updateUi()
                }

                override fun onLoaded() {
                    loadingDialog?.let {
                        adsProvider.showVideoAd(this)
                        it.dismiss()
                    }
                }

                override fun onFailedToLoad() {
                    noAdsDialog = DialogNoAds(baseActivity)
                    noAdsDialog?.show()
                    loadingDialog?.dismiss()
                }
            })
        }

        close.onClick { dismiss() }

        updateUi()
    }

    fun updateUi() {
        protonsCountText.text = dataProvider.getProtonsCount().toString()
        storeDialog?.updateUi()
    }
}
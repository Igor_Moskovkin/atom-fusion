package com.brain.training.atom.fusion.screen.base.presenter

interface Presenter<V> {

        fun onAttach(view: V?)

        fun onDetach()

        fun getView(): V?
}